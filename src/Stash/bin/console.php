#!/usr/bin/env php
<?php

require __DIR__.'/../../../vendor/autoload.php';
$container = require_once __DIR__ . '/../bootstrap.php';

use Symfony\Component\Console\Application;
use Stash\PullRequest\Command\ShowPullRequests;
use Stash\PullRequest\Command\PostComment;
use Stash\PullRequest\Command\Jenkins\GetPrForJob;
use Stash\PullRequest\Command\Jenkins\JobAction;
use Stash\PullRequest\Command\ApprovePullRequest;
use Stash\PullRequest\Command\UnApprovePullRequest;
use Stash\Configuration\Command\ImportConfigCommand;
use Stash\Configuration\Command\ValidateConfigCommand;

$application = new Application();

//  todo can be improved
$application
    ->addCommands([
        new ShowPullRequests($container),
        new PostComment($container),
        new GetPrForJob($container),
        new JobAction($container),
        new ApprovePullRequest($container),
        new UnApprovePullRequest($container),
        new ImportConfigCommand($container),
        new ValidateConfigCommand($container),
        new \Stash\PullRequest\Command\Changes($container),
        new \Stash\PullRequest\Command\MergeChecker($container),
        new \Stash\PullRequest\Command\Merge($container)
    ])
;

$application->run();
