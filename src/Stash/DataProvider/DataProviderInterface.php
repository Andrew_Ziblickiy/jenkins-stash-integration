<?php

namespace Stash\DataProvider;

/**
 * Interface DataProviderInterface
 * @package Stash\DataProvider
 */
interface DataProviderInterface
{
	/**
	 * @param int|string $id
	 * @return object
	 */
	public function findById($id);
	
	/**
	 * @return object
	 */
	public function findAll();
}
