<?php

namespace Stash\DataProvider;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Stash\ClientAwareTrait;
use Stash\Configuration\ConfigurationInterface;

/**
 * Class AbstractDataProvider
 * @package Stash\DataProvider
 */
abstract class AbstractDataProvider implements DataProviderInterface
{
	use ClientAwareTrait;
	
	/**
	 * @var ConfigurationInterface
	 */
	protected $configuration;
	
	/**
	 * @return ConfigurationInterface
	 */
	public function getConfiguration()
	{
		return $this->configuration;
	}
	
	/**
	 * @param ConfigurationInterface $configuration
	 */
	public function setConfiguration(ConfigurationInterface $configuration)
	{
		$this->configuration = $configuration;
	}
}
