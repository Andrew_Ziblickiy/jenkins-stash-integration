<?php

namespace Stash\DataMapper;


class DataMapper implements DataMapperInterface
{
	/**
	 * @var DataMapperInterface[]
	 */
	protected $dataMappers = [];
	
	/**
	 * @param DataMapperInterface $dataMapper
	 */
	public function addDataMapper(DataMapperInterface $dataMapper)
	{
		$this->dataMappers[] = $dataMapper;
	}
	
	/**
	 * @param string $className
	 * @return bool
	 */
	public function canMap(string $className): bool
	{
		foreach ($this->dataMappers as $mapper) {
			if ($mapper->canMap($className)) {
				return true;
			}
		}
		return false;
	}
	
	public function map(array $data)
	{
		foreach ($this->dataMappers as $mapper) {
		}
	}
}
