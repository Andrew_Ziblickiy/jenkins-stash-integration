<?php

namespace Stash\DataMapper;

/**
 * Interface DataMapperInterface
 * @package Stash\DataMapper
 */
interface DataMapperInterface extends CanMapInterface
{
	/**
	 * @param array $data
	 * @return object
	 */
	public function map(array $data);
}
