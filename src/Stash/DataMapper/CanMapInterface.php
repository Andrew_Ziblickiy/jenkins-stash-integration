<?php

namespace Stash\DataMapper;

/**
 * Interface CanMapInterface
 * @package Stash\DataMapper
 */
interface CanMapInterface
{
	/**
	 * @param string $className
	 * @return bool
	 */
	public function canMap(string $className) : bool;
}
