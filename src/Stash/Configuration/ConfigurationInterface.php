<?php

namespace Stash\Configuration;

/**
 * Interface ConfigurationInterface
 * @package Stash\Configuration
 */
interface ConfigurationInterface extends ApiConfigurationAwareInterface, ProjectConfigurationAwareInterface
{

}
