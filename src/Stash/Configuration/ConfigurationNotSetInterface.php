<?php
/**
 * Created by PhpStorm.
 * User: oncesk
 * Date: 28.07.17
 * Time: 16:20
 */

namespace Stash\Configuration;

/**
 * Interface ConfigurationNotSetInterface
 * @package Stash\Configuration
 */
interface ConfigurationNotSetInterface
{
	
}
