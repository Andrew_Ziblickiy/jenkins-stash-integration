<?php

namespace Stash\Configuration;

/**
 * Class InvalidApiConfiguration
 * @package Stash\Configuration
 */
class InvalidApiConfiguration extends ApiConfiguration implements ConfigurationNotSetInterface
{
	/**
	 * InvalidApiConfiguration constructor.
	 *
	 * @param string $host
	 * @param string $version
	 */
	public function __construct(string $host = '', string $version = '1.0')
	{
		parent::__construct($host, $version);
	}
}
