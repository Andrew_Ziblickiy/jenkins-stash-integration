<?php
/**
 * Created by PhpStorm.
 * User: oncesk
 * Date: 28.07.17
 * Time: 16:04
 */

namespace Stash\Configuration;

/**
 * Interface ConfigurationInterface
 * @package Stash
 */
interface ApiConfigurationInterface
{
	/**
	 * Should return stash base url
	 *
	 * @return string
	 */
	public function getHost() : string;
	
	/**
	 * Should return stash api version
	 *
	 * @return string
	 */
	public function getApiVersion() : string;
	
	/**
	 * @return string
	 */
	public function getUsername() : string;
	
	/**
	 * @return string
	 */
	public function getPassword() : string;
}
