<?php

namespace Stash\Configuration;

/**
 * Class ProjectConfiguration
 * @package Stash\Configuration
 */
class ProjectConfiguration implements ProjectConfigurationInterface
{
	/**
	 * @var string
	 */
	protected $key;
	
	/**
	 * @var string
	 */
	protected $slug;
	
	/**
	 * ProjectConfiguration constructor.
	 * @param string $key
	 * @param string $slug
	 */
	public function __construct(string $key, string $slug)
	{
		$this->key = $key;
		$this->slug = $slug;
	}
	
	/**
	 * @return string
	 */
	public function getProjectKey(): string
	{
		return $this->key;
	}
	
	/**
	 * @return string
	 */
	public function getRepositorySlug(): string
	{
		return $this->slug;
	}
}
