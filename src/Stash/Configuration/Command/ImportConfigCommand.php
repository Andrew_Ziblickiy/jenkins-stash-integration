<?php

namespace Stash\Configuration\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class ImportConfigCommand
 * @package Stash\Configuration
 */
class ImportConfigCommand extends Command
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
	/**
	 * ImportConfigCommand constructor.
	 * @param ContainerInterface $container
	 * @param null $name
	 */
	public function __construct(ContainerInterface $container, $name = null)
	{
		parent::__construct($name);
		$this->container = $container;
	}
	
	
	protected function configure()
	{
		$this
			->setName("conf:import")
			->setDescription("Import custom services and parameters to the application")
			->addArgument('parameters', InputArgument::REQUIRED, 'Application Parameters')
			->addArgument('services', InputArgument::OPTIONAL, 'Import custom services')
		;
	}
	
	public function execute(InputInterface $input, OutputInterface $output)
	{
		$fileSystem = $this->getFileSystem();
		$cacheConfigRoot = $this->getCacheConfigRoot();
		
		$sourceParameters = $input->getArgument('parameters');
		$parametersFile = $cacheConfigRoot . '/parameters.yml';
		
		$fileSystem->mkdir($cacheConfigRoot);
		$io = new SymfonyStyle($input, $output);
		
		$output->writeln("Parameters source file");
		$output->writeln("<info>${sourceParameters}</info>");
		$output->writeln("Copy passed params to the <info>${parametersFile}</info>");
		
		try {
			$fileSystem->copy($sourceParameters, $parametersFile, true);
		} catch (IOException $exception) {
			$io->error($exception->getMessage());
		} catch (FileNotFoundException $exception) {
			$io->error($exception->getMessage());
		}
		
	}
	
	/**
	 * @return Filesystem
	 */
	protected function getFileSystem()
	{
		return $this->container->get('filesystem');
	}
	
	/**
	 * @return string
	 */
	protected function getCacheConfigRoot()
	{
		return $this->container->getParameter('cache.config.root');
	}
}
