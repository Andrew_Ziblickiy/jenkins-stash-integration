<?php

namespace Stash\Configuration\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ValidateConfigCommand
 * @package Stash\Configuration\Command
 */
class ValidateConfigCommand extends Command
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
	/**
	 * ValidateConfigCommand constructor.
	 * @param ContainerInterface $container
	 * @param null $name
	 */
	public function __construct(ContainerInterface $container, $name = null)
	{
		parent::__construct($name);
		$this->container = $container;
	}
	
	protected function configure()
	{
		$this
			->setName('conf:validate')
			->setDescription('Validate current configuration')
		;
	}
	
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$parametersForCheck = [
			'stash.api.host', 'stash.api.version', 'stash.api.username', 'stash.api.password', 'stash.project.key',
			'stash.project.name'
		];
		
		$io = new SymfonyStyle($input, $output);
		
		$table = new Table($output);
		$table->setHeaders([
			'Parameter',
			'Is set',
			'Value'
		]);
		$output->writeln("Check if required parameters are set");
		$hasErrors = false;
		
		foreach ($parametersForCheck as $param) {
			$isSet = $this->container->hasParameter($param);
			$table->addRow([
				$param,
				($isSet ? 'yes' : 'no'),
				($isSet ? $this->container->getParameter($param) : 'not set')
			]);
			
			if (!$isSet) {
				$hasErrors = true;
			}
		}
		
		$table->render();
		
		if ($hasErrors) {
			$io->error("Please pass parameters");
		}
		
		$output->writeln("Trying to fetch Configuration object");
		try {
			$conf = $this->container->get('configuration');
			$output->writeln("Done");
		} catch (\Exception $e) {
			$io->error($e->getMessage());
		}
		
		$output->writeln("<info>It seems like all configuration is correct</info>");
	}
}
