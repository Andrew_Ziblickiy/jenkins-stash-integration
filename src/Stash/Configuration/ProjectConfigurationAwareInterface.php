<?php

namespace Stash\Configuration;

/**
 * Interface ProjectConfigurationAwareInterface
 * @package Stash\Configuration
 */
interface ProjectConfigurationAwareInterface
{
	/**
	 * @return ProjectConfigurationInterface
	 */
	public function getProjectConfiguration() : ProjectConfigurationInterface;
	
	/**
	 * @param ProjectConfigurationInterface $configuration
	 * @return $this
	 */
	public function setProjectConfiguration(ProjectConfigurationInterface $configuration);
}
