<?php

namespace Stash\Configuration;

/**
 * Class InvalidProjectConfiguration
 * @package Stash\Configuration
 */
class InvalidProjectConfiguration extends ProjectConfiguration implements ConfigurationNotSetInterface
{
	/**
	 * InvalidProjectConfiguration constructor.
	 * @param string $key
	 * @param string $slug
	 */
	public function __construct(string $key = '', string $slug = '')
	{
		parent::__construct($key, $slug);
	}
	
}
