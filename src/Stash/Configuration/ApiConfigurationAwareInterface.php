<?php

namespace Stash\Configuration;

/**
 * Interface ApiConfigurationAwareInterface
 * @package Stash\Configuration
 */
interface ApiConfigurationAwareInterface
{
	/**
	 * @return ApiConfigurationInterface
	 */
	public function getApiConfiguration() : ApiConfigurationInterface;
	
	/**
	 * @param ApiConfigurationInterface $configuration
	 * @return $this
	 */
	public function setApiConfiguration(ApiConfigurationInterface $configuration);
}
