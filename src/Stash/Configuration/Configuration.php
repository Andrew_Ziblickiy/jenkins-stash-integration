<?php

namespace Stash\Configuration;

/**
 * Class Configuration
 * @package Stash\Configuration
 */
class Configuration implements ConfigurationInterface
{
	use ApiConfigurationAwareTrait;
	use ProjectConfigurationAwareTrait;
	
	/**
	 * Configuration constructor.
	 * @param ApiConfigurationInterface $apiConfiguration
	 * @param ProjectConfigurationInterface $projectConfiguration
	 */
	public function __construct(ApiConfigurationInterface $apiConfiguration, ProjectConfigurationInterface $projectConfiguration)
	{
		$this->setApiConfiguration($apiConfiguration);
		$this->setProjectConfiguration($projectConfiguration);
	}
}
