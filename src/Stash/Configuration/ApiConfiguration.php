<?php

namespace Stash\Configuration;

/**
 * Class ApiConfiguration
 * @package Stash\Configuration
 */
class ApiConfiguration implements ApiConfigurationInterface
{
	/**
	 * @var string
	 */
	protected $host;
	
	/**
	 * @var string
	 */
	protected $version;
	
	/**
	 * @var string
	 */
	protected $username;
	
	/**
	 * @var string
	 */
	protected $password;
	
	/**
	 * ApiConfiguration constructor.
	 * @param string $host
	 * @param string $version
	 * @param string $username
	 * @param string $password
	 */
	public function __construct(string $host, string $version = '1.0', string $username, string $password)
	{
		$this->host = $host;
		$this->version = $version;
		$this->username = $username;
		$this->password = $password;
	}
	
	/**
	 * @return string
	 */
	public function getHost(): string
	{
		return $this->host;
	}
	
	/**
	 * @return string
	 */
	public function getApiVersion(): string
	{
		return $this->version;
	}
	
	/**
	 * @return string
	 */
	public function getUsername(): string
	{
		return $this->username;
	}
	
	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}
}
