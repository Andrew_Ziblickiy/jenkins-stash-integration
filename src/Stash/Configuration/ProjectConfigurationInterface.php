<?php

namespace Stash\Configuration;

/**
 * Interface ProjectConfigurationInterface
 * @package Stash\Configuration
 */
interface ProjectConfigurationInterface
{
	/**
	 * @return string
	 */
	public function getProjectKey() : string;
	
	/**
	 * @return string
	 */
	public function getRepositorySlug() : string;
}
