<?php

namespace Stash\Configuration;

/**
 * Trait ApiConfigurationAwareTrait
 * @package Stash\Configuration
 */
trait ApiConfigurationAwareTrait
{
	/**
	 * @var ApiConfigurationInterface
	 */
	protected $apiConfiguration;
	
	/**
	 * @return ApiConfigurationInterface
	 */
	public function getApiConfiguration() : ApiConfigurationInterface
	{
		if (null == $this->apiConfiguration) {
			$this->apiConfiguration = new InvalidApiConfiguration();
		}
		
		return $this->apiConfiguration;
	}
	
	/**
	 * @param ApiConfigurationInterface $configuration
	 * @return $this
	 */
	public function setApiConfiguration(ApiConfigurationInterface $configuration)
	{
		$this->apiConfiguration = $configuration;
		return $this;
	}
}
