<?php

namespace Stash\Configuration;

/**
 * Trait ProjectConfigurationAwareTrait
 * @package Stash\Configuration
 */
trait ProjectConfigurationAwareTrait
{
	/**
	 * @var ProjectConfigurationInterface
	 */
	protected $projectConfiguration;
	
	/**
	 * @return ProjectConfigurationInterface
	 */
	public function getProjectConfiguration() : ProjectConfigurationInterface
	{
		if (null === $this->projectConfiguration) {
			$this->projectConfiguration = new InvalidProjectConfiguration();
		}
		
		return $this->projectConfiguration;
	}
	
	/**
	 * @param ProjectConfigurationInterface $configuration
	 * @return $this
	 */
	public function setProjectConfiguration(ProjectConfigurationInterface $configuration)
	{
		$this->projectConfiguration = $configuration;
		return $this;
	}
}
