<?php

namespace Stash;

use GuzzleHttp\ClientInterface;

/**
 * Trait ClientAwareTrait
 * @package Stash
 */
trait ClientAwareTrait
{
    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @return ClientInterface|null
     */
    public function getClient() : ClientInterface
    {
        return $this->client;
    }

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client)
    {
        $this->client = $client;
    }
}
