<?php

namespace Stash;

use GuzzleHttp\Client;
use Stash\Configuration\ApiConfigurationInterface;

/**
 * Class ClientFactory
 * @package Stash
 */
class ClientFactory
{
	/**
	 * @param ApiConfigurationInterface $configuration
	 *
	 * @return Client
	 */
	public static function create(ApiConfigurationInterface $configuration)
	{
		return new Client([
			'base_uri' => $configuration->getHost(),
			'auth' => [
				$configuration->getUsername(),
				$configuration->getPassword()
			]
		]);
	}
}