<?php

namespace Stash;

use GuzzleHttp\ClientInterface;

/**
 * Interface ClientAwareInterface
 * @package Stash
 */
interface ClientAwareInterface
{
    /**
     * @return ClientInterface|null
     */
    public function getClient() : ClientInterface;

    /**
     * @param ClientInterface $client
     */
    public function setClient(ClientInterface $client);
}
