<?php

namespace Stash\User;

/**
 * Interface UserInterface
 * @package Stash\User
 */
interface UserInterface
{
	/**
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * @return string
	 */
	public function getEmail() : string;
}
