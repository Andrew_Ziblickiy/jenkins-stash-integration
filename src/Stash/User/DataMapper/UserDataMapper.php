<?php

namespace Stash\User\DataMapper;

use Stash\DataMapper\DataMapperInterface;
use Stash\User\User;
use Stash\User\UserInterface;

/**
 * Class UserDataMapper
 * @package Stash\User\DataMapper
 */
class UserDataMapper implements DataMapperInterface
{
	/**
	 * @param string $className
	 * @return bool
	 */
	public function canMap(string $className): bool
	{
		return $className === UserInterface::class;
	}
	
	/**
	 * @param array $data
	 * @return UserInterface
	 */
	public function map(array $data)
	{
		//  todo improve this
		$user = new User($data['name'], $data['email'] ?? $data['emailAddress']);
		
		return $user;
	}
}
