<?php

namespace Stash\User;

/**
 * Class UserAwareTrait
 * @package Stash\User
 */
trait UserAwareTrait
{
	/**
	 * @var UserInterface
	 */
	protected $user;
	
	/**
	 * @return UserInterface
	 */
	public function getUser() : UserInterface
	{
		if (null === $this->user) {
			$this->user = new UndefinedUser();
		}
		
		return $this->user;
	}
	
	/**
	 * @param UserInterface $user
	 * @return $this
	 */
	public function setUser(UserInterface $user)
	{
		$this->user = $user;
		return $this;
	}
}
