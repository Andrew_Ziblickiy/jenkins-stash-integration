<?php

namespace Stash\User;

/**
 * Class RoleInterface
 * @package Stash\User
 */
interface RoleInterface
{
	const AUTHOR = 'AUTHOR';
	const REVIEWER = 'REVIEWER';
	const PARTICIPANT = 'PARTICIPANT';
}
