<?php

namespace Stash\User;

/**
 * Interface UserAwareInterface
 * @package Stash\User
 */
interface UserAwareInterface
{
	/**
	 * @return UserInterface
	 */
	public function getUser() : UserInterface;
	
	/**
	 * @param UserInterface $user
	 * @return $this
	 */
	public function setUser(UserInterface $user);
}
