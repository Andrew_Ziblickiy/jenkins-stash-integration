<?php

namespace Stash\User;

use Stash\Common\IdAwareInterface;
use Stash\Common\IdAwareTrait;

/**
 * Class IdentifiableUser
 * @package Stash\User
 */
class IdentifiableUser extends User implements IdAwareInterface
{
	use IdAwareTrait;
	
	/**
	 * IdentifiableUser constructor.
	 * @param string|int $id
	 * @param string $name
	 * @param $email
	 */
	public function __construct($id, string $name, string $email)
	{
		parent::__construct($name, $email);
		$this->setId($id);
	}
}
