<?php
/**
 * Created by PhpStorm.
 * User: oncesk
 * Date: 28.07.17
 * Time: 17:06
 */

namespace Stash\User;

/**
 * Class User
 * @package Stash\User
 */
class User implements UserInterface
{
	/**
	 * @var string
	 */
	protected $name;
	
	/**
	 * @var string
	 */
	protected $email;
	
	/**
	 * User constructor.
	 * @param string $name
	 * @param string $email
	 */
	public function __construct(string $name, string $email)
	{
		$this->name = $name;
		$this->email = $email;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	/**
	 * @return string
	 */
	public function getEmail(): string
	{
		return $this->email;
	}
}
