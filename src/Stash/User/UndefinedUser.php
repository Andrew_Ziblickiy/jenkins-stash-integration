<?php

namespace Stash\User;

use Stash\Property\NotPassedInterface;

/**
 * Class UndefinedUser
 * @package Stash\User
 */
class UndefinedUser extends User implements NotPassedInterface
{
	/**
	 * UndefinedUser constructor.
	 * @param string $id
	 * @param string $name
	 * @param string $email
	 */
	public function __construct($id = '-1', $name = 'Not Passed', $email = 'Not Passed')
	{
		parent::__construct($id, $name, $email);
	}
}
