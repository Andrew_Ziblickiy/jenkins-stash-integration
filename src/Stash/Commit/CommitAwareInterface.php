<?php

namespace Stash\Commit;

/**
 * Class CommitAwareInterface
 * @package Stash\Commit
 */
interface CommitAwareInterface
{
    /**
     * @return CommitInterface
     */
    public function getCommit() : CommitInterface;

    /**
     * @param CommitInterface $commit
     * @return $this
     */
    public function setCommit(CommitInterface $commit);
}
