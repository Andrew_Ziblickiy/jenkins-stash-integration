<?php

namespace Stash\Commit;

use Stash\Common\IdAwareInterface;
use Stash\User\UserAwareInterface;

/**
 * Interface CommitInterface
 * @package Stash\Commit
 */
interface CommitInterface extends IdAwareInterface
{

}
