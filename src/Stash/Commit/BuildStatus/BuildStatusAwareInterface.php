<?php

namespace Stash\Commit\BuildStatus;

/**
 * Interface BuildStatusAwareInterface
 * @package Stash\Commit\BuildStatus
 */
interface BuildStatusAwareInterface
{
	/**
	 * @return BuildStatusInterface|null
	 */
	public function getBuildStatus();
	
	/**
	 * @param BuildStatusInterface $status
	 * @return $this
	 */
	public function setBuildStatus(BuildStatusInterface $status);
}
