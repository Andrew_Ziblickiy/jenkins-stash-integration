<?php

namespace Stash\Commit\BuildStatus;

/**
 * Trait BuildStatusAwareTrait
 * @package Stash\Commit\BuildStatus
 */
trait BuildStatusAwareTrait
{
	/**
	 * @var BuildStatusInterface
	 */
	protected $buildStatus;
	
	/**
	 * @return BuildStatusInterface|null
	 */
	public function getBuildStatus()
	{
		return $this->buildStatus;
	}
	
	/**
	 * @param BuildStatusInterface $status
	 * @return $this
	 */
	public function setBuildStatus(BuildStatusInterface $status)
	{
		$this->buildStatus = $status;
		
		return $this;
	}
}
