<?php

namespace Stash\Commit\BuildStatus;

use Stash\Commit\CommitInterface;
use Stash\Configuration\ApiConfigurationInterface;

/**
 * Class EndPoint
 * @package Stash\Commit\BuildStatus
 */
class EndPoint implements EndPointInterface
{
	/**
	 * @var ApiConfigurationInterface
	 */
	protected $apiConfiguration;
	
	/**
	 * EndPoint constructor.
	 * @param ApiConfigurationInterface $configuration
	 */
	public function __construct(ApiConfigurationInterface $configuration)
	{
		$this->apiConfiguration = $configuration;
	}
	
	/**
	 * @param CommitInterface $commit
	 * @return string
	 */
	public function getCommitEndPoint(CommitInterface $commit)
	{
		return sprintf(
			"/rest/build-status/%s/commits/%s",
			$this->apiConfiguration->getApiVersion(),
			$commit->getId()
		);
	}
}
