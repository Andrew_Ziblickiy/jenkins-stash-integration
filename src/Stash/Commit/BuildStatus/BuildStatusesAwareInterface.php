<?php

namespace Stash\Commit\BuildStatus;

/**
 * Interface BuildStatusesAwareInterface
 * @package Stash\Commit\BuildStatus
 */
interface BuildStatusesAwareInterface
{
    /**
     * @param BuildStatusInterface $buildStatus
     * @return $this
     */
    public function addBuildStatus(BuildStatusInterface $buildStatus);

    /**
     * @return BuildStatusInterface[]
     */
    public function getBuildStatuses() : array;
}
