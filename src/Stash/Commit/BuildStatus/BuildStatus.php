<?php

namespace Stash\Commit\BuildStatus;

use Stash\Commit\CommitAwareInterface;
use Stash\Commit\CommitAwareTrait;

/**
 * Class BuildStatus
 * @package Stash\Commit\BuildStatus
 */
class BuildStatus implements BuildStatusInterface, CommitAwareInterface, \Serializable
{
    use CommitAwareTrait;

	/**
	 * @var string
	 */
	protected $state;
	
	/**
	 * @var string
	 */
	protected $key;
	
	/**
	 * @var string
	 */
	protected $name;
	
	/**
	 * @var string
	 */
	protected $url;
	
	/**
	 * @var string
	 */
	protected $description;
	
	/**
	 * BuildStatus constructor.
	 * @param string $state
	 * @param string $key
	 * @param string $name
	 * @param string $url
	 * @param string $description
	 */
	public function __construct(string $state, string $key, string $name, string $url, string $description)
	{
		//  todo we should validate state, maybe create ValueObject for it
		$this->state = $state;
		$this->key = $key;
		$this->name = $name;
		$this->url = $url;
		$this->description = $description;
	}
	
	/**
	 * @return string
	 */
	public function getState(): string
	{
		return $this->state;
	}
	
	/**
	 * @param string $state
	 * @return $this
	 */
	public function setState(string $state)
	{
		$this->state = $state;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getKey(): string
	{
		return $this->key;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->name;
	}
	
	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}
	
	/**
	 * @return string
	 */
	public function getDescription(): string
	{
		return $this->description;
	}
	
	/**
	 * @return string
	 */
	public function serialize()
	{
		return serialize([
			'state' => $this->getState(),
			'key' => $this->getKey(),
			'name' => $this->getName(),
			'url' => $this->getUrl(),
			'description' => $this->getDescription(),
            'commit' => $this->getCommit()
		]);
	}
	
	/**
	 * @param string $serialized
	 */
	public function unserialize($serialized)
	{
		$attrs = unserialize($serialized);
		
		foreach ($attrs as $attr => $value) {
			$this->$attr = $value;
		}
	}
}
