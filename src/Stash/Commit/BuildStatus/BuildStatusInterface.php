<?php

namespace Stash\Commit\BuildStatus;

/**
 * Interface BuildStatusInterface
 * @package Stash\Commit\BuildStatus
 */
interface BuildStatusInterface
{
	const STATE_SUCCESSFUL = 'SUCCESSFUL';
	const STATE_FAILED = 'FAILED';
	const STATE_INPROGRESS = 'INPROGRESS';
	
	/**
	 * @return string
	 */
	public function getState() : string;
	
	/**
	 * @param string $state
	 * @return $this
	 */
	public function setState(string $state);
	
	/**
	 * @return string
	 */
	public function getKey() : string;
	
	/**
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * @return string
	 */
	public function getUrl() : string;
	
	/**
	 * @return string
	 */
	public function getDescription() : string;
}
