<?php

namespace Stash\Commit\BuildStatus;

use Stash\Commit\CommitInterface;

/**
 * Interface EndPointInterface
 * @package Stash\Commit\BuildStatus
 */
interface EndPointInterface
{
	/**
	 * @param CommitInterface $commit
	 * @return string
	 */
	public function getCommitEndPoint(CommitInterface $commit);
}
