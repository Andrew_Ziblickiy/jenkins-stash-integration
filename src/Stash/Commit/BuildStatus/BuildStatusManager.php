<?php

namespace Stash\Commit\BuildStatus;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Stash\Commit\CommitInterface;

/**
 * Class Manager
 * @package Stash\Commit\BuildStatus
 */
class BuildStatusManager implements BuildStatusManagerInterface
{
	/**
	 * @var Client|ClientInterface
	 */
	protected $client;
	
	/**
	 * @var EndPointInterface
	 */
	protected $endPoint;
	
	/**
	 * Manager constructor.
	 * @param ClientInterface $client
	 * @param EndPointInterface $endPoint
	 */
	public function __construct(ClientInterface $client, EndPointInterface $endPoint)
	{
		$this->client = $client;
		$this->endPoint = $endPoint;
	}
	
	/**
	 * @param CommitInterface $commit
	 * @return BuildStatusInterface[]
	 */
	public function getStatuses(CommitInterface $commit)
	{
		//  todo implement that
		return [];
	}
	
	/**
	 * @param BuildStatusInterface $status
	 * @param CommitInterface $commit
	 * @return bool
	 */
	public function associateStatusWithCommit(BuildStatusInterface $status, CommitInterface $commit)
	{
	    try {
            $response = $this->client->post(
                $this->endPoint->getCommitEndPoint($commit),
                [
                    'json' => [
                        "state" => $status->getState(),
                        "key" => $status->getKey(),
                        "name" => $status->getName(),
                        "url" => $status->getUrl(),
                        "description" => $status->getDescription()
                    ]
                ]
            );
        } catch (\Exception $e) {
	        return false;
        }
		
		return in_array($response->getStatusCode(), [200, 204]);
	}
}
