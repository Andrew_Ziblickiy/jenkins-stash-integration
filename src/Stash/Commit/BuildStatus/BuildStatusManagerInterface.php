<?php

namespace Stash\Commit\BuildStatus;

use Stash\Commit\CommitInterface;

/**
 * Interface BuildStatusManagerInterface
 * @package Stash\Commit\BuildStatus
 */
interface BuildStatusManagerInterface
{
	/**
	 * @param CommitInterface $commit
	 * @return BuildStatusInterface[]
	 */
	public function getStatuses(CommitInterface $commit);
	
	/**
	 * @param BuildStatusInterface $status
	 * @param CommitInterface $commit
	 * @return bool
	 */
	public function associateStatusWithCommit(BuildStatusInterface $status, CommitInterface $commit);
}
