<?php

namespace Stash\Commit;

use Stash\Property\NotPassedInterface;

/**
 * Class CommitAwareTrait
 * @package Stash\Commit
 */
trait CommitAwareTrait
{
    /**
     * @var CommitInterface
     */
    protected $commit;

    /**
     * @return CommitInterface
     */
    public function getCommit() : CommitInterface
    {
        if (null === $this->commit) {
            $this->commit = new UndefinedCommit();
        }

        return $this->commit;
    }

    /**
     * @param CommitInterface $commit
     * @return $this
     */
    public function setCommit(CommitInterface $commit)
    {
        $this->commit = $commit;

        return $this;
    }
}
