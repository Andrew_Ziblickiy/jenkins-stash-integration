<?php

namespace Stash\Commit;

use Stash\Property\NotPassedInterface;

/**
 * Class UndefinedCommit
 * @package Stash\Commit
 */
class UndefinedCommit extends Commit implements NotPassedInterface
{
    public function __construct()
    {
        $this->setId('UNDEF');
    }

}
