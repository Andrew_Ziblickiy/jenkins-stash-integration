<?php

namespace Stash\Commit;

use Stash\Common\IdAwareTrait;

/**
 * Class Commit
 * @package Stash\Commit
 */
class Commit implements CommitInterface, \Serializable
{
	use IdAwareTrait;
	
	/**
	 * Commit constructor.
	 * @param string $id
	 */
	public function __construct(string $id)
	{
		$this->setId($id);
	}

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize([
            'id' => $this->getId()
        ]);
    }

    /**
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $attrs = unserialize($serialized);

        foreach ($attrs as $attr => $value) {
            $this->$attr = $value;
        }
    }
}
