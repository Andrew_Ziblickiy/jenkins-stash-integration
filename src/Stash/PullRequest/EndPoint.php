<?php

namespace Stash\PullRequest;

use Stash\Configuration\ApiConfigurationAwareInterface;
use Stash\Configuration\ApiConfigurationInterface;
use Stash\Configuration\ConfigurationInterface;
use Stash\Configuration\ProjectConfigurationAwareInterface;
use Stash\Configuration\ProjectConfigurationInterface;

/**
 * Class EndPoint
 * @package Stash\PullRequest
 */
class EndPoint implements EndPointInterface
{
	/**
	 * @var ProjectConfigurationInterface
	 */
	protected $projectConfiguration;
	
	/**
	 * @var ApiConfigurationInterface
	 */
	protected $apiConfiguration;
	
	/**
	 * EndPoint constructor.
	 * @param ProjectConfigurationInterface|ProjectConfigurationAwareInterface|ConfigurationInterface|ApiConfigurationInterface|ApiConfigurationAwareInterface $configuration
	 */
	public function __construct($configuration)
	{
		if ($configuration instanceof ProjectConfigurationInterface) {
			$this->projectConfiguration = $configuration;
		} else if ($configuration instanceof ProjectConfigurationAwareInterface) {
			$this->projectConfiguration = $configuration->getProjectConfiguration();
		} else {
			throw new \RuntimeException('You must pass ProjectConfigurationInterface|ProjectConfigurationAwareInterface');
		}
		
		if ($configuration instanceof ApiConfigurationInterface) {
			$this->apiConfiguration = $configuration;
		} else if ($configuration instanceof ApiConfigurationAwareInterface) {
			$this->apiConfiguration = $configuration->getApiConfiguration();
		} else {
			throw new \RuntimeException(
				'You must pass ProjectConfigurationInterface|ProjectConfigurationAwareInterface|ConfigurationInterface|ApiConfigurationInterface|ApiConfigurationAwareInterface'
			);
		}
	}
	
	/**
	 * @return string
	 */
	public function getPullRequestsEndPoint(): string
	{
		return sprintf(
			"/rest/api/%s/projects/%s/repos/%s/pull-requests",
			$this->getApiConfiguration()->getApiVersion(),
			$this->getProjectConfiguration()->getProjectKey(),
			$this->getProjectConfiguration()->getRepositorySlug()
		);
	}
	
	/**
	 * @param int|string $id
	 * @return string
	 */
	public function getPullRequestEndPoint($id): string
	{
		return sprintf(
			'/rest/api/%s/projects/%s/repos/%s/pull-requests/%s',
			$this->getApiConfiguration()->getApiVersion(),
			$this->getProjectConfiguration()->getProjectKey(),
			$this->getProjectConfiguration()->getRepositorySlug(),
			$id
		);
	}
	
	/**
	 * @param int|string $id
	 * @return string
	 */
	public function getPullRequestApproveEndPoint($id): string
	{
		return sprintf(
			'/rest/api/%s/projects/%s/repos/%s/pull-requests/%s/participants/%s',
			$this->getApiConfiguration()->getApiVersion(),
			$this->getProjectConfiguration()->getProjectKey(),
			$this->getProjectConfiguration()->getRepositorySlug(),
			$id,
			$this->getApiConfiguration()->getUsername()
		);
	}
	
	/**
	 * @param int|string $id
	 * @return string
	 */
	public function getPullRequestCommentsEndPoint($id): string
	{
		return sprintf(
			'/rest/api/%s/projects/%s/repos/%s/pull-requests/%s/comments',
			$this->getApiConfiguration()->getApiVersion(),
			$this->getProjectConfiguration()->getProjectKey(),
			$this->getProjectConfiguration()->getRepositorySlug(),
			$id
		);
	}
	
	/**
	 * @param $id
	 * @param $commentId
	 * @return string
	 */
	public function getPullRequestCommentEndPoint($id, $commentId) : string
	{
		return sprintf(
			'/rest/api/%s/projects/%s/repos/%s/pull-requests/%s/comments/%s',
			$this->getApiConfiguration()->getApiVersion(),
			$this->getProjectConfiguration()->getProjectKey(),
			$this->getProjectConfiguration()->getRepositorySlug(),
			$id,
			$commentId
		);
	}

    /**
     * @param $id
     * @return string
     */
    public function getPullRequestChangesEndPoint($id): string
    {
        return sprintf(
            '/rest/api/%s/projects/%s/repos/%s/pull-requests/%s/changes',
            $this->getApiConfiguration()->getApiVersion(),
            $this->getProjectConfiguration()->getProjectKey(),
            $this->getProjectConfiguration()->getRepositorySlug(),
            $id
        );
    }

    /**
     * @param $id
     * @return string
     */
    public function getPullRequestActivitiesEndPoint($id): string
    {
        return sprintf(
            '/rest/api/%s/projects/%s/repos/%s/pull-requests/%s/activities',
            $this->getApiConfiguration()->getApiVersion(),
            $this->getProjectConfiguration()->getProjectKey(),
            $this->getProjectConfiguration()->getRepositorySlug(),
            $id
        );
    }

    /**
     * @param $id
     * @return string
     */
    public function getParticipants($id): string
    {
        return sprintf(
            '/rest/api/%s/projects/%s/repos/%s/pull-requests/%s/participants',
            $this->getApiConfiguration()->getApiVersion(),
            $this->getProjectConfiguration()->getProjectKey(),
            $this->getProjectConfiguration()->getRepositorySlug(),
            $id
        );
    }

    /**
     * @param $id
     * @return string
     */
    public function getMergeEndPoint($id): string
    {
        return sprintf(
            '/rest/api/%s/projects/%s/repos/%s/pull-requests/%s/merge',
            $this->getApiConfiguration()->getApiVersion(),
            $this->getProjectConfiguration()->getProjectKey(),
            $this->getProjectConfiguration()->getRepositorySlug(),
            $id
        );
    }

    /**
	 * @return ApiConfigurationInterface
	 */
	protected function getApiConfiguration()
	{
		return $this->apiConfiguration;
	}
	
	/**
	 * @return ProjectConfigurationInterface
	 */
	protected function getProjectConfiguration()
	{
		return $this->projectConfiguration;
	}
}
