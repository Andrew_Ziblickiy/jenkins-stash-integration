<?php

namespace Stash\PullRequest;

use Stash\User\UserAwareInterface;

/**
 * Interface ReviewerInterface
 * @package Stash\PullRequest
 */
interface ReviewerInterface extends UserAwareInterface
{
	/**
	 * @return bool
	 */
	public function isApproved() : bool;
}
