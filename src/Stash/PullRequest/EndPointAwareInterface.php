<?php

namespace Stash\PullRequest;

/**
 * Class EndPointAwareInterface
 * @package Stash\PullRequest
 */
interface EndPointAwareInterface
{
    /**
     * @return EndPointInterface|null
     */
    public function getEndPoint() : EndPointInterface;

    /**
     * @param EndPointInterface $endPoint
     */
    public function setEndPoint(EndPointInterface $endPoint);
}
