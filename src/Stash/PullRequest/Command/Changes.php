<?php

namespace Stash\PullRequest\Command;

use Stash\DataProvider\DataProviderInterface;
use Stash\PullRequest\Comment\Comment;
use Stash\PullRequest\DataProvider\PullRequestDataProvider;
use Stash\PullRequest\PullRequestInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApprovePullRequest
 * @package Stash\PullRequest\Command
 */
class Changes extends Command
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
	/**
	 * ApprovePullRequest constructor.
	 * @param ContainerInterface $container
	 * @param null $name
	 */
	public function __construct(ContainerInterface $container, $name = null)
	{
		parent::__construct($name);
		$this->container = $container;
	}
	
	protected function configure()
	{
		$this
			->setName('pr:changes')
			->setDescription('Show pull request changes')
			->addArgument('id', InputArgument::REQUIRED, 'Pull request id')
            ->addOption('only-files', null, InputOption::VALUE_NONE, '')
            ->addOption('skip-deleted', null, InputOption::VALUE_NONE, '')
            ->addOption('base-path', null, InputOption::VALUE_OPTIONAL, '', '')
			->addOption('ext', null, InputOption::VALUE_REQUIRED)
		;
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$pr = $this->getPrDataProvider()->findById($input->getArgument('id'));
		
		if (!$pr) {
			$output->writeln('<error>Could not find pull request</error>');
			return 1;
		} else {
            $changeSet = $this->getPullRequestManaget()->getChangeSet($pr);

            if ($input->getOption('only-files')) {
                $files = [];
                $basePath = $input->getOption('base-path');
                $skipDeleted = $input->getOption('skip-deleted');
                $ext = $input->getOption('ext');

                foreach ($changeSet->getChanges() as $change) {

                	if ($ext && pathinfo($change->getPath(), PATHINFO_EXTENSION) != $ext) {
                		continue;
	                }

                    if ($skipDeleted) {
                        if (!$change->getChangeType()->isDeleted()) {
                            $files[] = $basePath . $change->getPath();
                        }
                    } else {
                        $files[] = $basePath . $change->getPath();
                    }
                }

                $output->write(implode(' ', $files));
            }
		}
	}
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @param PullRequestManagerInterface $manager
	 *
	 * @return bool
	 */
	protected function doAction(PullRequestInterface $pullRequest, PullRequestManagerInterface $manager)
	{
		return $manager->approve($pullRequest);
	}
	
	/**
	 * @return DataProviderInterface|PullRequestDataProvider
	 */
	protected function getPrDataProvider()
	{
		return $this->container->get('pr.dataprovider');
	}
	
	/**
	 * @return PullRequestManagerInterface
	 */
	protected function getPullRequestManaget()
	{
		return $this->container->get('pr.manager');
	}
}
