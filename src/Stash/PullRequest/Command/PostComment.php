<?php

namespace Stash\PullRequest\Command;

use Stash\DataProvider\DataProviderInterface;
use Stash\PullRequest\Comment\Comment;
use Stash\PullRequest\DataProvider\PullRequestDataProvider;
use Stash\PullRequest\PullRequestInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ApprovePullRequest
 * @package Stash\PullRequest\Command
 */
class PostComment extends Command
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
	/**
	 * ApprovePullRequest constructor.
	 * @param ContainerInterface $container
	 * @param null $name
	 */
	public function __construct(ContainerInterface $container, $name = null)
	{
		parent::__construct($name);
		$this->container = $container;
	}
	
	protected function configure()
	{
		$this
			->setName('pr:comment:post')
			->setDescription('Post comment for pull request')
			->addArgument('id', InputArgument::REQUIRED, 'Pull request id')
			->addArgument('message', InputArgument::REQUIRED, 'Message')
			->addOption('file', 'f', InputOption::VALUE_OPTIONAL, 'Set message file')
		;
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 *
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$pr = $this->getPrDataProvider()->findById($input->getArgument('id'));
		
		if (!$pr) {
			$output->writeln('<error>Could not find pull request</error>');
			return 1;
		} else {
			$manager = $this->getPullRequestManaget();
			$message = $input->getArgument('message');
			
			if ($input->hasOption('file')) {
				$file = $input->getOption('file');
				if (file_exists($file)) {
					$loadedFile = file($file, FILE_SKIP_EMPTY_LINES);
					$message .= "\n```\n";
					$message .= join("\n", array_slice($loadedFile, -15));
					$message .= "\n```";
				}
			}
			
			$result = $manager->addComment(new Comment($pr, $message));
			
			if ($result) {
				$output->writeln("<info>Success</info>");
			} else {
				$output->writeln("<error>Could not post message</error>");
				return 2;
			}
		}
	}
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @param PullRequestManagerInterface $manager
	 *
	 * @return bool
	 */
	protected function doAction(PullRequestInterface $pullRequest, PullRequestManagerInterface $manager)
	{
		return $manager->approve($pullRequest);
	}
	
	/**
	 * @return DataProviderInterface|PullRequestDataProvider
	 */
	protected function getPrDataProvider()
	{
		return $this->container->get('pr.dataprovider');
	}
	
	/**
	 * @return PullRequestManagerInterface
	 */
	protected function getPullRequestManaget()
	{
		return $this->container->get('pr.manager');
	}
}
