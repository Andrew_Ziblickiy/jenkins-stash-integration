<?php

namespace Stash\PullRequest\Command;

use Psr\Container\ContainerInterface;
use Stash\DataProvider\DataProviderInterface;
use Stash\PullRequest\DataProvider\PullRequestDataProvider;
use Stash\PullRequest\PullRequestInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Merge
 * @package Stash\PullRequest\Command
 */
class Merge extends Command
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ApprovePullRequest constructor.
     * @param ContainerInterface $container
     * @param null $name
     */
    public function __construct(ContainerInterface $container, $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName('pr:merge')
            ->setDescription('Merge pull request')
            ->addArgument('id', InputArgument::REQUIRED, 'Pull request id')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pr = $this->getPrDataProvider()->findById($input->getArgument('id'));

        if (!$pr) {
            $output->writeln('<error>Could not find pull request</error>');
            return 1;
        }

        if ($this->isPullRequestReadyForMerge($pr, $output)) {
            $pullRequestManager = $this->getPullRequestManager();

            return (int) !$pullRequestManager->merge($pr);
        }

        if ($output instanceof ConsoleOutputInterface) {
            $output->getErrorOutput()->writeln('<error>Pull request is not ready for merge</error>');
        }

        return 1;
    }

    /**
     * @param PullRequestInterface $pullRequest
     * @param OutputInterface $output
     * @return bool
     */
    protected function isPullRequestReadyForMerge(PullRequestInterface $pullRequest, OutputInterface $output) : bool
    {
        $command = $this->getApplication()->find('pr:merge:check');
        $arguments = array(
            'command' => 'pr:merge:check',
            'id'    => $pullRequest->getId()
        );

        return !$command->run(new ArrayInput($arguments), $output);
    }

    /**
     * @return PullRequestManagerInterface
     */
    protected function getPullRequestManager()
    {
        return $this->container->get('pr.manager');
    }

    /**
     * @return DataProviderInterface|PullRequestDataProvider
     */
    protected function getPrDataProvider()
    {
        return $this->container->get('pr.dataprovider');
    }
}
