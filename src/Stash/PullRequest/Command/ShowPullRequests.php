<?php

namespace Stash\PullRequest\Command;

use Psr\Container\ContainerInterface;
use Stash\DataProvider\DataProviderInterface;
use Stash\PullRequest\DataProvider\PullRequestDataProvider;
use Stash\PullRequest\PullRequestInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableCell;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ShowPullRequests
 * @package Stash\PullRequest\Command
 */
class ShowPullRequests extends Command
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
	/**
	 * ShowPullRequests constructor.
	 * @param ContainerInterface $container
	 * @param null $name
	 */
	public function __construct(ContainerInterface $container, $name = null)
	{
		parent::__construct($name);
		$this->container = $container;
	}
	
	protected function configure()
	{
		$this
			->setName('pr:show')
			->setDescription('Show list of pull requests or detailed information about single one')
			->addArgument('id', InputArgument::OPTIONAL, 'Pull request id')
			->addOption('format', 'f', InputOption::VALUE_REQUIRED, 'Format output, works only for one pr, you can use: {.ID} {.SB} {.SCI} {.DB} {.DCI}')
		;
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$dataProvider = $this->getPrDataProvider();
		
		$id = $input->getArgument('id');
		
		if ($id) {
			$this->printPullRequest(
				$dataProvider->findById($id),
				$input,
				$output
			);
		} else {
			$prs = $dataProvider->findAll();
			
			$table = new Table($output);
			$table->setHeaders([
				'Id',
				'Title',
				'State',
				'Author',
				'Source Branch',
				'Destination Branch',
			]);
			
			foreach ($prs as $pr) {
				$table->addRow([
					$pr->getId(),
					$pr->getTitle(),
					$pr->getState(),
					$pr->getAuthor()->getUser()->getName(),
					$pr->getFromRef()->getDisplayId(),
					$pr->getToRef()->getDisplayId()
				]);
			}
			$table->addRow(new TableSeparator());
			$table->addRow(['Total', new TableCell(count($prs), ['colspan' => 5])]);
			$table->render();
		}
	}

	/**
	 * @param PullRequestInterface $pullRequest
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	protected function printPullRequest(PullRequestInterface $pullRequest, InputInterface $input, OutputInterface $output)
	{
		$format = $input->getOption('format');

		if ($format) {
			$this->printPullRequestWithFormat($pullRequest, $format, $output);
		} else {
			$this->printPullRequestAsTable($pullRequest, $output);
		}
	}

	/**
	 * @param PullRequestInterface $pullRequest
	 * @param string $format
	 * @param OutputInterface $output
	 */
	protected function printPullRequestWithFormat(PullRequestInterface $pullRequest, string $format, OutputInterface $output)
	{
		$output->write(
			str_replace(
				[
					'{.ID}',
					'{.SCI}',
					'{.SB}',
					'{.DCI}',
					'{.DB}'
				],
				[
					$pullRequest->getId(),
					$pullRequest->getFromRef()->getLatestChangeset(),
					$pullRequest->getFromRef()->getDisplayId(),
					$pullRequest->getToRef()->getLatestChangeset(),
					$pullRequest->getToRef()->getDisplayId()
				],
				$format
			)
		);
	}

	/**
	 * @param PullRequestInterface $pullRequest
	 * @param OutputInterface $output
	 */
	protected function printPullRequestAsTable(PullRequestInterface $pullRequest, OutputInterface $output)
	{
		$table = new Table($output);

		$table->setHeaders([
			'Property',
			'Value'
		]);

		$table->addRow([
			'ID',
			$pullRequest->getId()
		]);

		$table->addRow([
			'Title',
			$pullRequest->getTitle()
		]);

		$table->addRow([
			'Author Name',
			$pullRequest->getAuthor()->getUser()->getName()
		]);

		$table->addRow([
			'Author Email',
			$pullRequest->getAuthor()->getUser()->getEmail()
		]);

		$table->addRow([
			'Source Branch',
			$pullRequest->getFromRef()->getDisplayId()
		]);

		$table->addRow([
			'Source Branch Commit',
			$pullRequest->getFromRef()->getLatestChangeset()
		]);

		$table->addRow([
			'Destination Branch',
			$pullRequest->getToRef()->getDisplayId()
		]);

		$table->addRow([
			'Destination Branch Commit',
			$pullRequest->getToRef()->getLatestChangeset()
		]);

		$table->render();
	}
	
	/**
	 * @return DataProviderInterface|PullRequestDataProvider
	 */
	protected function getPrDataProvider()
	{
		return $this->container->get('pr.dataprovider');
	}
}
