<?php

namespace Stash\PullRequest\Command;

use Psr\Container\ContainerInterface;
use Stash\DataProvider\DataProviderInterface;
use Stash\PullRequest\DataProvider\PullRequestDataProvider;
use Stash\PullRequest\MergeValidator\ApproversMergeValidator;
use Stash\PullRequest\MergeValidator\BitbucketConstraintsMergeValidator;
use Stash\PullRequest\MergeValidator\CompositeMergeValidator;
use Stash\PullRequest\MergeValidatorInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class MergeChecker
 * @package Stash\PullRequest\Command
 */
class MergeChecker extends Command
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * ApprovePullRequest constructor.
     * @param ContainerInterface $container
     * @param null $name
     */
    public function __construct(ContainerInterface $container, $name = null)
    {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this
            ->setName('pr:merge:check')
            ->setDescription('Check if pull request can be merged')
            ->addArgument('id', InputArgument::REQUIRED, 'Pull request id')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $pr = $this->getPrDataProvider()->findById($input->getArgument('id'));

        if (!$pr) {
            $output->writeln('<error>Could not find pull request</error>');
            return 1;
        } else {
            $validator = $this->createMergeValidator();
            $this->configureMergeValidator($validator);

            return (int) !$validator->isPullRequestCanBeMerged($pr);
        }
    }

    /**
     * @return MergeValidatorInterface
     */
    protected function createMergeValidator() : MergeValidatorInterface
    {
        $validator = new CompositeMergeValidator();
        $validator->setClient($this->container->get('http_client'));
        $validator->setEndPoint($this->container->get('pr.endpoint'));

        return $validator;
    }

    /**
     * @param MergeValidatorInterface|CompositeMergeValidator $validator
     */
    protected function configureMergeValidator(MergeValidatorInterface $validator)
    {
        $validator
            ->addMergeValidator(new ApproversMergeValidator($this->getImportantApprovers()))
            ->addMergeValidator(new BitbucketConstraintsMergeValidator())
        ;
    }

    /**
     * @return array
     */
    protected function getImportantApprovers() : array
    {
        return [
            'nlsvc-jenkins-docker',
            'uladkala',
            'katsutli',
            'siarandr'
        ];
    }

    /**
     * @return DataProviderInterface|PullRequestDataProvider
     */
    protected function getPrDataProvider()
    {
        return $this->container->get('pr.dataprovider');
    }
}
