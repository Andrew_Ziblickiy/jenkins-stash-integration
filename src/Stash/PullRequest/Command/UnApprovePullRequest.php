<?php

namespace Stash\PullRequest\Command;

use Stash\PullRequest\PullRequestInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Symfony\Component\Console\Input\InputArgument;

/**
 * Class ApprovePullRequest
 * @package Stash\PullRequest\Command
 */
class UnApprovePullRequest extends ApprovePullRequest
{
	/**
	 *
	 */
	protected function configure()
	{
		$this
			->setName('pr:unapprove')
			->setDescription('Remove Approve from pull request')
			->addArgument('id', InputArgument::REQUIRED, 'Pull request id')
		;
	}
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @param PullRequestManagerInterface $manager
	 * @return bool
	 */
	protected function doAction(PullRequestInterface $pullRequest, PullRequestManagerInterface $manager)
	{
		return $manager->unApprove($pullRequest);
	}
}
