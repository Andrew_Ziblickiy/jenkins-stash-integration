<?php

namespace Stash\PullRequest\Command\Jenkins\Event;

use Stash\PullRequest\Jenkins\BuildInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class JobActionEvent
 * @package Stash\PullRequest\Command\Jenkins\Event
 */
class JobActionEvent extends Event
{
	const NAME_START = 'jenkins.build.start';
	const NAME_STOP = 'jenkins.build.stop';
	
	/**
	 * @var BuildInterface
	 */
	protected $build;
	
	/**
	 * @var InputInterface
	 */
	protected $input;
	
	/**
	 * JobActionEvent constructor.
	 * @param BuildInterface $build
	 * @param InputInterface $input
	 */
	public function __construct(BuildInterface $build, InputInterface $input)
	{
		$this->build = $build;
		$this->input = $input;
	}
	
	/**
	 * @return BuildInterface
	 */
	public function getBuild(): BuildInterface
	{
		return $this->build;
	}
	
	/**
	 * @return InputInterface
	 */
	public function getInput(): InputInterface
	{
		return $this->input;
	}
}
