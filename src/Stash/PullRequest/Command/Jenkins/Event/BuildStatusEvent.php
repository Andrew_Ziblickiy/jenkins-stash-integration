<?php

namespace Stash\PullRequest\Command\Jenkins\Event;

use Stash\PullRequest\Jenkins\BuildInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class BuildStatusEvent
 * @package Stash\PullRequest\Command\Jenkins\Event
 */
class BuildStatusEvent extends Event
{
	const NAME = 'jenkins.build.status';

	/**
	 * @var boolean
	 */
	private $status;

	/**
	 * @var BuildInterface
	 */
	private $build;

	/**
	 * @var InputInterface
	 */
	private $input;

	/**
	 * @var OutputInterface
	 */
	private $output;

	/**
	 * BuildStatusEvent constructor.
	 * @param bool $status
	 * @param BuildInterface $build
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 */
	public function __construct(bool $status, BuildInterface $build, InputInterface $input, OutputInterface $output)
	{
		$this->status = $status;
		$this->build = $build;
		$this->input = $input;
		$this->output =$output;
	}

	/**
	 * @return bool
	 */
	public function isSuccess(): bool
	{
		return true === $this->status;
	}

	/**
	 * @return bool
	 */
	public function isError() : bool
	{
		return false === $this->status;
	}

	/**
	 * @return BuildInterface
	 */
	public function getBuild() : BuildInterface
	{
		return $this->build;
	}

	/**
	 * @return InputInterface
	 */
	public function getInput(): InputInterface
	{
		return $this->input;
	}

	/**
	 * @return OutputInterface
	 */
	public function getOutput(): OutputInterface
	{
		return $this->output;
	}
}
