<?php

namespace Stash\PullRequest\Command\Jenkins;

use Stash\DataProvider\DataProviderInterface;
use Stash\PullRequest\DataProvider\PullRequestDataProvider;
use Stash\PullRequest\Jenkins\Build;
use Stash\PullRequest\Jenkins\BuildInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class BaseCommand
 * @package Stash\PullRequest\Command\Jenkins
 */
class BaseCommand extends Command
{
	/**
	 * @var ContainerInterface
	 */
	protected $container;
	
	/**
	 * ApprovePullRequest constructor.
	 * @param ContainerInterface $container
	 * @param null $name
	 */
	public function __construct(ContainerInterface $container, $name = null)
	{
		parent::__construct($name);
		$this->container = $container;
	}
	
	/**
	 * @return DataProviderInterface|PullRequestDataProvider
	 */
	protected function getPrDataProvider()
	{
		return $this->container->get('pr.dataprovider');
	}
	
	/**
	 * @return Filesystem
	 */
	protected function getFileSystem()
	{
		return $this->container->get('filesystem');
	}
	
	/**
	 * @return string
	 */
	protected function getCacheFolder()
	{
		return $this->container->getParameter('cache.root') . '/pr';
	}
	
	/**
	 * @param string $id
	 * @param string $cacheFolder
	 * @return string
	 */
	protected function makeCacheFileName($id, string $cacheFolder)
	{
		return sprintf(
			"%s/%s.serialized",
			$cacheFolder,
			$id
		);
	}

    /**
     * @param int $id
     *
     * @return BuildInterface
     */
	protected function getBuildByPullRequestId(int $id)
    {
        try {
            $build = $this->loadBuild($id);
        } catch (\LogicException $exception) {
            $build = new Build($this->getPrDataProvider()->findById($id));
            $this->saveBuild($build);
        }

        return $build;
    }
	
	/**
	 * @param $id
	 *
	 * @return BuildInterface
	 */
	protected function loadBuild($id)
	{
		$cacheFile = $this->makeCacheFileName($id, $this->getCacheFolder());
		
		if (!file_exists($cacheFile)) {
			throw new \LogicException('Requested build does not exists');
		}
		
		return unserialize(file_get_contents($cacheFile));
	}
	
	/**
	 * @param BuildInterface $build
	 */
	protected function saveBuild(BuildInterface $build)
	{
		$cacheFile = $this->makeCacheFileName($build->getPullRequestId(), $this->getCacheFolder());
		
		return $this->getFileSystem()->dumpFile($cacheFile, serialize($build));
	}
	
	/**
	 * @param BuildInterface $build
	 */
	protected function removeBuild(BuildInterface $build)
	{
		return $this->getFileSystem()->remove($this->makeCacheFileName($build->getPullRequestId(), $this->getCacheFolder()));
	}
}
