<?php

namespace Stash\PullRequest\Command\Jenkins;

use Stash\Commit\BuildStatus\BuildStatusAwareInterface;
use Stash\Commit\BuildStatus\BuildStatusInterface;
use Stash\Commit\BuildStatus\BuildStatusManagerInterface;
use Stash\Commit\Commit;
use Stash\PullRequest\Comment\Comment;
use Stash\PullRequest\Jenkins\Build;
use Stash\PullRequest\Jenkins\BuildCollection;
use Stash\PullRequest\Jenkins\BuildCollectionInterface;
use Stash\PullRequest\Jenkins\BuildInterface;
use Stash\PullRequest\Jenkins\BuildSelectorInterface;
use Stash\PullRequest\Jenkins\BuildStoreInterface;
use Stash\PullRequest\Jenkins\Exception\BuildNotFoundException;
use Stash\PullRequest\Jenkins\PullRequestAwareBuild;
use Stash\PullRequest\PullRequestAwareInterface;
use Stash\PullRequest\PullRequestInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Stash\PullRequest\UndefinedPullRequest;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\NullOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GetPrForJenkinsJob
 * @package Stash\PullRequest\Command
 */
class GetPrForJob extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('pr:jenkins:get-id')
            ->setDescription('Get pull request id for jenkins job')
            ->addOption(
                'pull-request',
                'pr',
                InputOption::VALUE_OPTIONAL,
                'Pull request ID, which should be returned with some format'
            )
            ->addOption(
                'format',
                'f',
                InputOption::VALUE_OPTIONAL,
                'Format output, example "pid:scid" will be printed pullrequestid:sourcecommitid',
                'pid'
            )
            ->addOption(
                'build-not-found-phrase',
                'bnfp',
                InputOption::VALUE_OPTIONAL,
                'Return some phrase if build not found, by default return 2 exit code if you will pass phrase will be zero exit code and phraze in stdout'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $prDataProvider = $this->getPrDataProvider();
        $pullRequests = $prDataProvider->findAll();

        $buildStore = $this->getBuildStore();
        $collection = new BuildCollection();

        foreach ($pullRequests as $pullRequest) {
            if ($buildStore->has($pullRequest->getId())) {
                $build = $buildStore->load($pullRequest->getId());
                if ($build instanceof PullRequestAwareInterface) {
                    $build->setPullRequest($pullRequest);
                }
            } else {
                $build = new Build($pullRequest);
            }
            $collection->addBuild($build);
        }

        try {
            $build = null;
            $pullRequestIdOption = $input->getOption('pull-request');

            if ($pullRequestIdOption) {
                foreach ($collection->getBuilds() as $buildForCheck) {
                    if ($buildForCheck->getPullRequestId() == $pullRequestIdOption) {
                        $build = $buildForCheck;
                        break;
                    }
                }
            }

            if (null === $build) {
                $build = $this->getBuildSelector()->select($collection);
            }

            $buildStore->save($build);

            $pr = $prDataProvider->findById($build->getPullRequestId());

            $output->write(str_replace(
                [
                    '{.PID}',
                    '{.SCI}',
                    '{.DCI}',
                    '{.SB}',
                    '{.DB}'
                ],
                [
                    $build->getPullRequestId(),
	                $pr->getFromRef()->getLatestChangeset(),
                    $pr->getToRef()->getLatestChangeset(),
                    $pr->getFromRef()->getDisplayId(),
                    $pr->getToRef()->getDisplayId()
                ],
                $input->getOption('format')
            ));

            $status = 0;
        } catch (BuildNotFoundException $exception) {
            $phrase = $input->getOption('build-not-found-phrase');
            if ($phrase) {
                $output->write($phrase);
                $status = 0;
            } else {
                $output->writeln(sprintf(
                    "<error>%s</error>",
                    $exception->getMessage()
                ));
                $status = 2;
            }

        } catch (\LogicException $exception) {
            $output->writeln(sprintf(
                "<error>%s</error>",
                $exception->getMessage()
            ));
            $status = 1;
        } catch (\Exception $exception) {
            $output->writeln(sprintf(
                "<error>%s</error>",
                $exception->getMessage()
            ));
            $status = 3;
        } finally {
            $buildStore->cleanBuildsWhichIsNotInList($collection);
            $this->validateAndMakeBuildNotMergableOnDestBranchChanged($collection);
        }

        return $status;
    }

    /**
     * @return BuildStoreInterface
     */
    protected function getBuildStore()
    {
        return $this->container->get('pr.build.store');
    }

    /**
     * @return BuildSelectorInterface
     */
    protected function getBuildSelector()
    {
        return $this->container->get('pr.build.selector');
    }

	/**
	 * @param BuildCollectionInterface $buildCollection
	 */
    protected function validateAndMakeBuildNotMergableOnDestBranchChanged(BuildCollectionInterface $buildCollection)
    {
	    $manager = $this->getPullRequestManager();
	    $buildStatusManager = $this->getBuildStatusManager();
	    $buildStore = $this->getBuildStore();


	    /** @var PullRequestAwareInterface|BuildInterface $build */
	    foreach ($buildCollection->getBuilds() as $build) {

	    	if (!$build->isSuccess()) {
	    		continue;
		    }

		    if ($build->isInProgress()) {
	    		continue;
		    }

		    if ($this->isBuildValidForLocking($build)) {
			    $pr = $build->getPullRequest();

			    if ($build->getParameters()->has('rebuild.' . $pr->getToRef()->getLatestChangeset())) {
			    	continue;
			    }

			    try {

				    $this->postNotificationMessageAboutUnAvailabilityOfMerge(
					    $pr,
					    $build,
					    $manager
				    );

			    	$command = $this->getApplication()->find('pr:unapprove');
			    	$command->run(new ArrayInput([
			    		'command' => 'pr:unapprove',
					    'id' => $build->getPullRequestId()
				    ]), new NullOutput());

				    if ($build instanceof BuildStatusAwareInterface) {
					    $status = $build->getBuildStatus();

					    if ($status) {
						    $status->setState(BuildStatusInterface::STATE_FAILED);
						    $buildStatusManager->associateStatusWithCommit(
							    $status,
							    new Commit($build->getSourceBranchCommit())
						    );
					    }
				    }

				    $build->getParameters()->set('rebuild.' . $pr->getToRef()->getLatestChangeset(), true);

				    $buildStore->save($build);
			    } catch (\Exception $e) {

			    }
		    }
	    }
    }

    protected function postNotificationMessageAboutUnAvailabilityOfMerge(
    	PullRequestInterface $pr,
	    BuildInterface $build,
	    PullRequestManagerInterface $pullRequestManager
    ) {
	    $message =<<<MSG
    
Unfortunately, this pull request can not be merged to [%s] because of [%s] branch was modified or source branch was changed and you should wait for the build with newer version of destination branch.

=> Destination
Branch:                 %s
Current commit:         %s
Was build with commit:  %s

=> Source
Branch:                 %s
Current commit:         %s
Was build with commit:  %s

⚠ This pull request will be marked as failed also unapproved by jenkins.

MSG;

	    $message = sprintf(
		    $message,
		    $pr->getToRef()->getDisplayId(),
		    $pr->getToRef()->getDisplayId(),
		    $pr->getToRef()->getDisplayId(),
		    $pr->getToRef()->getLatestChangeset(),
		    $build->getDestinationBranchCommit(),

            $pr->getFromRef()->getDisplayId(),
            $pr->getFromRef()->getLatestChangeset(),
            $build->getSourceBranchCommit()
	    );

	    $comment = new Comment(
		    new UndefinedPullRequest($build->getPullRequestId()),
		    $message
	    );

	    $pullRequestManager->addComment($comment);
    }

	/**
	 * @param PullRequestInterface $pullRequest
	 * @return bool
	 */
    protected function isValidPullRequest(PullRequestInterface $pullRequest)
    {
    	return !$pullRequest instanceof UndefinedPullRequest;
    }

	/**
	 * @param PullRequestInterface $pullRequest
	 * @param BuildInterface $build
	 * @return bool
	 */
    protected function isPullRequestDestinationBranchCommitHasChangesWithinTheBuild(
    	PullRequestInterface $pullRequest,
		BuildInterface $build
    ) {
    	$destinationBranchReference = $pullRequest->getToRef();

    	return $destinationBranchReference->getDisplayId() == $build->getDestinationBranch()
		    && $destinationBranchReference->getLatestChangeset() != $build->getDestinationBranchCommit();
    }

    /**
     * @param PullRequestInterface $pullRequest
     * @param BuildInterface $build
     * @return bool
     */
    protected function isPullRequestSourceBrancheHasChangesWithinTheBuild(
        PullRequestInterface $pullRequest,
        BuildInterface $build
    ) {
        $sourceBranchReference = $pullRequest->getFromRef();

        return $sourceBranchReference->getDisplayId() == $build->getSourceBranch()
            && $sourceBranchReference->getLatestChangeset() != $build->getSourceBranchCommit();
    }

    protected function isBuildValidForLocking(BuildInterface $build)
    {
	    if (!$build instanceof PullRequestAwareInterface) {
		    return false;
	    }

	    $pr = $build->getPullRequest();

	    if ($this->isValidPullRequest($pr) && ($this->isPullRequestDestinationBranchCommitHasChangesWithinTheBuild($pr, $build) || $this->isPullRequestSourceBrancheHasChangesWithinTheBuild($pr, $build))) {
	        return true;
        }

        return false;
    }

	/**
	 * @return PullRequestManagerInterface
	 */
	protected function getPullRequestManager() : PullRequestManagerInterface
	{
		return $this->container->get('pr.manager');
	}

	/**
	 * @return BuildStatusManagerInterface
	 */
	protected function getBuildStatusManager() : BuildStatusManagerInterface
	{
		return $this->container->get('commit.build_status.manager');
	}
}
