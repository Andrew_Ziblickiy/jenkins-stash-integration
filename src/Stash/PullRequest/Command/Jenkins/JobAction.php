<?php

namespace Stash\PullRequest\Command\Jenkins;

use Stash\PullRequest\Command\Jenkins\Event\BuildStatusEvent;
use Stash\PullRequest\Command\Jenkins\Event\JobActionEvent;
use Stash\PullRequest\Comment\Comment;
use Stash\PullRequest\Jenkins\Build;
use Stash\PullRequest\Jenkins\BuildInterface;
use Stash\PullRequest\PullRequestAwareInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Stash\PullRequest\UndefinedPullRequest;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Class StartJob
 * @package Stash\PullRequest\Command\Jenkins
 */
class JobAction extends BaseCommand
{
	protected function configure()
	{
		$this
			->setName('pr:jenkins:job')
			->setDescription('Start build on jenkins and set internal build to the inProgress state')
			->addArgument('id', InputArgument::REQUIRED, 'Pull request Id')
			->addArgument('action', InputArgument::REQUIRED, 'Action (start|stop)')
			->addOption('success', 'su', InputOption::VALUE_NONE, 'Is Success')
			->addOption('error', 'err', InputOption::VALUE_NONE, 'Is Error')
			->addOption('message', 'm', InputOption::VALUE_OPTIONAL, 'Message about status')
			->addOption('messageFile', 'mf', InputOption::VALUE_OPTIONAL, 'Message file log about status')
            ->addOption('messageFileLength', 'mfl', InputOption::VALUE_OPTIONAL, 'Read passed length of message file', -15)
            ->addOption('messageFileFullLength', 'mffl', InputOption::VALUE_OPTIONAL, 'Pass all file content to the commit', false)
			->addOption('buildId', 'bi', InputOption::VALUE_OPTIONAL)
		;
	}
	
	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		try {
			$build = $this->getBuildByPullRequestId($input->getArgument('id'));
            $event = new JobActionEvent($build, $input);
            $eventName = null;

			switch ($input->getArgument('action')) {
				case 'start':
					$eventName = JobActionEvent::NAME_START;
					$build->getParameters()->set('BUILD_ID', $input->getOption('buildId'));
					break;
					
				case 'stop':
					$eventName = JobActionEvent::NAME_STOP;
					break;
					
				case 'remove':
					if (!$this->removeBuild($build)) {
						throw new \Exception('Could not remove build');
					}
					return 0;
					break;
			}

			if ($eventName) {
                $this->getEventDispatcher()->dispatch($eventName, $event);
            }

			$this->saveBuild($build);
		} catch (\Exception $exception) {
			$output->writeln("<error>" . $exception->getMessage() . "</error>");
			return 1;
		}
	}
	
	protected function tailFile(string $file, int $offset = -15, bool $fullFile = false) {
	    var_dump([
	        $file,
            $offset,
            $fullFile
        ]);
		if (file_exists($file)) {
		    if ($fullFile) {
		        return substr(file_get_contents($file), -15000);
            }

			$loadedFile = file($file, FILE_SKIP_EMPTY_LINES);
			return join("\n", array_slice($loadedFile, $offset));
		}
		
		return "";
	}
	
	/**
	 * @return EventDispatcherInterface
	 */
	protected function getEventDispatcher()
	{
		return $this->container->get('event_dispatcher');
	}
}
