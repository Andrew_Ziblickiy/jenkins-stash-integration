<?php

namespace Stash\PullRequest;

use Stash\PullRequest\Comment\CommentInterface;
use Stash\PullRequest\Jenkins\BuildInterface;

/**
 * Interface BuildCommentProviderInterface
 * @package Stash\PullRequest
 */
interface BuildCommentProviderInterface
{
    /**
     * @param BuildInterface $build
     * @return CommentInterface
     */
    public function getBuildComment(BuildInterface $build) : CommentInterface;
}
