<?php

namespace Stash\PullRequest;

/**
 * Interface EndPointInterface
 * @package Stash\PullRequest
 */
interface EndPointInterface
{
	/**
	 * @return string
	 */
	public function getPullRequestsEndPoint() : string;
	
	/**
	 * @param string|int $id
	 * @return string
	 */
	public function getPullRequestEndPoint($id) : string;
	
	/**
	 * @param $id
	 * @return string
	 */
	public function getPullRequestApproveEndPoint($id) : string;
	
	/**
	 * @param $id
	 * @return string
	 */
	public function getPullRequestCommentsEndPoint($id) : string;
	
	/**
	 * @param $id
	 * @param $commentId
	 * @return string
	 */
	public function getPullRequestCommentEndPoint($id, $commentId) : string;

    /**
     * @param $id
     * @return string
     */
	public function getPullRequestChangesEndPoint($id) : string;

    /**
     * @param $id Pullrequest id
     * @return string
     */
	public function getPullRequestActivitiesEndPoint($id) : string;

    /**
     * @param $id
     * @return string
     */
	public function getParticipants($id) : string;

    /**
     * @param $id
     * @return string
     */
	public function getMergeEndPoint($id) : string;
}
