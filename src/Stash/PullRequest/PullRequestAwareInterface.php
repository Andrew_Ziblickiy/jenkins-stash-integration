<?php

namespace Stash\PullRequest;

/**
 * Interface PullRequestAwareInterface
 * @package Stash\PullRequest
 */
interface PullRequestAwareInterface
{
	/**
	 * @return PullRequestInterface
	 */
	public function getPullRequest() : PullRequestInterface;
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @return $this
	 */
	public function setPullRequest(PullRequestInterface $pullRequest);
}
