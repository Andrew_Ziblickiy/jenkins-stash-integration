<?php

namespace Stash\PullRequest\DataProvider;

use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Stash\DataMapper\DataMapperInterface;
use Stash\DataProvider\AbstractDataProvider;
use Stash\PullRequest\EndPointInterface;
use Stash\PullRequest\PullRequestInterface;

/**
 * Class PullRequestDataProvider
 * @package Stash\PullRequest\DataProvider
 */
class PullRequestDataProvider extends AbstractDataProvider
{
	/**
	 * @var EndPointInterface
	 */
	protected $endPoint;
	
	/**
	 * @var DataMapperInterface
	 */
	protected $dataMapper;
	
	/**
	 * PullRequestDataProvider constructor.
	 * @param EndPointInterface $endPoint
	 * @param DataMapperInterface $dataMapper
	 */
	public function __construct(EndPointInterface $endPoint, DataMapperInterface $dataMapper)
	{
		$this->endPoint = $endPoint;
		if (!$dataMapper->canMap(PullRequestInterface::class)) {
			throw new \LogicException('Passed data mapper could not map data to class with interface PullRequestInterface::class');
		}
		$this->dataMapper = $dataMapper;
	}
	
	/**
	 * @param int|string $id
	 *
	 * @return PullRequestInterface
	 */
	public function findById($id)
	{
		$response = $this
			->getClient()
			->get($this->endPoint->getPullRequestEndPoint($id))
		;
		
		if ($response->getStatusCode() == 200) {
			try {
				$json = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
				return $this->dataMapper->map($json);
			} catch (\Exception $e) {}
		}
		
		return null;
	}
	
	/**
	 * @return PullRequestInterface[]
	 */
	public function findAll()
	{
		$response = $this->getClient()->get($this->endPoint->getPullRequestsEndPoint());
		
		$objects = [];
		if ($response->getStatusCode() == 200) {
			$content = $response->getBody()->getContents();

			try {
				$json = \GuzzleHttp\json_decode($content, true);
				
				foreach ($json['values'] as $item) {
					$objects[] = $this->dataMapper->map($item);
				}
			} catch (\Exception $e) {}
		}
		return $objects;
	}
}
