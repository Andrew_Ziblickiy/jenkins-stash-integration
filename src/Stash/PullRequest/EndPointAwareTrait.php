<?php

namespace Stash\PullRequest;

/**
 * Trait EndPointAwareTrait
 * @package Stash\PullRequest
 */
trait EndPointAwareTrait
{
    /**
     * @var EndPointInterface
     */
    private $endPoint;

    /**
     * @return EndPointInterface|null
     */
    public function getEndPoint() : EndPointInterface
    {
        return $this->endPoint;
    }

    /**
     * @param EndPointInterface $endPoint
     */
    public function setEndPoint(EndPointInterface $endPoint)
    {
        $this->endPoint = $endPoint;
    }
}
