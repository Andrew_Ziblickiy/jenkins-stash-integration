<?php

namespace Stash\PullRequest\Comment;

use Stash\Common\IdAwareInterface;
use Stash\Common\IdAwareTrait;
use Stash\PullRequest\PullRequestAwareTrait;
use Stash\PullRequest\PullRequestInterface;

/**
 * Class Comment
 * @package Stash\PullRequest\Comment
 */
class Comment implements CommentInterface, IdAwareInterface
{
	use PullRequestAwareTrait;
	use IdAwareTrait;
	
	/**
	 * @var string
	 */
	protected $message;
	
	/**
	 * Comment constructor.
	 * @param PullRequestInterface $pullRequest
	 * @param string $message
	 */
	public function __construct(PullRequestInterface $pullRequest, string $message)
	{
		$this->setPullRequest($pullRequest);
		$this->message = $message;
	}
	
	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->message;
	}
}
