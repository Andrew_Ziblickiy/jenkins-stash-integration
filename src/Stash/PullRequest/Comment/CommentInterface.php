<?php

namespace Stash\PullRequest\Comment;

use Stash\PullRequest\PullRequestInterface;

/**
 * Interface CommentInterface
 * @package Stash\PullRequest\Comment
 */
interface CommentInterface
{
	
	/**
	 * @return PullRequestInterface
	 */
	public function getPullRequest() : PullRequestInterface;
	
	/**
	 * @return string
	 */
	public function getMessage() : string;
}
