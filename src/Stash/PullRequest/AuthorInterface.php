<?php

namespace Stash\PullRequest;

use Stash\User\UserAwareInterface;

/**
 * Interface AuthorInterface
 * @package Stash\PullRequest
 */
interface AuthorInterface extends UserAwareInterface
{
	/**
	 * @return string
	 */
	public function getRole() : string;
	
	/**
	 * @return bool
	 */
	public function isApproved() : bool;
}
