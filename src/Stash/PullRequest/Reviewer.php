<?php

namespace Stash\PullRequest;

use Stash\User\UserAwareTrait;

/**
 * Class Reviewer
 * @package Stash\PullRequest
 */
class Reviewer implements ReviewerInterface
{
    use UserAwareTrait;

    /**
     * @var bool
     */
    private $approved;

    /**
     * Reviewer constructor.
     * @param bool $approved
     */
    public function __construct(bool $approved = false)
    {
        $this->approved = $approved;
    }

    /**
     * @return bool
     */
    public function isApproved(): bool
    {
        return $this->approved;
    }
}
