<?php

namespace Stash\PullRequest;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use Stash\Common\IdAwareInterface;
use Stash\PullRequest\Changes\Change;
use Stash\PullRequest\Changes\ChangeSet;
use Stash\PullRequest\Changes\ChangeSetInterface;
use Stash\PullRequest\Changes\ChangeType;
use Stash\PullRequest\Comment\CommentInterface;

/**
 * Class PullRequestManager
 * @package Stash\PullRequest
 */
class PullRequestManager implements PullRequestManagerInterface
{
	/**
	 * @var ClientInterface|Client
	 */
	protected $client;
	
	/**
	 * @var EndPointInterface
	 */
	protected $endPoint;
	
	/**
	 * PullRequestManager constructor.
	 * @param ClientInterface $client
	 */
	public function __construct(ClientInterface $client, EndPointInterface $endPoint)
	{
		$this->client = $client;
		$this->endPoint = $endPoint;
	}
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @return bool
	 */
	public function approve(PullRequestInterface $pullRequest): bool
	{
		$response = $this->client->put(
			$this->endPoint->getPullRequestApproveEndPoint($pullRequest->getId()),
			[
				'json' => [
					'approved' => true,
					'status' => 'APPROVED'
				]
			]
		);
		
		return $response->getStatusCode() == 200;
	}
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @return bool
	 */
	public function unApprove(PullRequestInterface $pullRequest): bool
	{
		$response = $this->client->put(
			$this->endPoint->getPullRequestApproveEndPoint($pullRequest->getId()),
			[
				'json' => [
					'status' => 'UNAPPROVED',
					'approved' => false

				]
			]
		);
		
		return $response->getStatusCode() == 200;
	}

    /**
     * @param PullRequestInterface $pullRequest
     * @return bool
     */
    public function merge(PullRequestInterface $pullRequest): bool
    {
        try {
            $response = $this->client->post(
                $this->endPoint->getMergeEndPoint($pullRequest->getId()) . '?version=' . $pullRequest->getVersion(),
                [
                    'headers' => [
                        'X-Atlassian-Token' => 'no-check'
                    ]
                ]
            );
        } catch (GuzzleException $e) {

            if ($e instanceof RequestException) {
                print_r($pullRequest);
                print_r(json_decode($e->getResponse()->getBody()->getContents(), true));
            }

            return false;
        }

        return $response->getStatusCode() == 200;
    }

    /**
     * @param PullRequestInterface $pullRequest
     * @return ChangeSetInterface
     */
    public function getChangeSet(PullRequestInterface $pullRequest): ChangeSetInterface
    {
        //  todo move logic to separate class ....
        $start = 0;
        $limit = 5;
        $json = [];

        do {
            $url = $this->endPoint->getPullRequestChangesEndPoint($pullRequest->getId()) . "?start=$start&limit=$limit";

            $response = $this->client->get(
                $url
            );

            if ($response->getStatusCode() == 200) {
                $changes = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
                $json = array_merge($json, $changes['values']);
            } else {
                throw new \RuntimeException($response->getReasonPhrase(), 1);
            }

            $start += $limit;

        } while ($changes['isLastPage'] == 0);

        $changeSet = new ChangeSet();

        foreach ($json as $item) {
            $change = new Change($item);
            $change->setChangeType(new ChangeType($item));

            $changeSet->add($change);
        }

        return $changeSet;
    }

    /**
	 * @param CommentInterface $comment
	 * @return bool
	 */
	public function addComment(CommentInterface $comment): bool
	{
		$response = $this->client->post(
			$this->endPoint->getPullRequestCommentsEndPoint($comment->getPullRequest()->getId()),
			[
				'json' => [
					'text' => $comment->getMessage()
				]
			]
		);
		
		$status = in_array($response->getStatusCode(), [200, 201]);
		
		if ($comment instanceof IdAwareInterface) {
			//  trying to fetch id and set it
			if ($status) {
				$json = $response->getBody()->getContents();
				try {
					$json = \GuzzleHttp\json_decode($json, true);
					$comment->setId($json['id'] ?? null);
				} catch (\InvalidArgumentException $e) {}
			}
		}
		
		return $status;
	}
	
	/**
	 * @param CommentInterface $comment
	 * @return bool
	 */
	public function updateCommend(CommentInterface $comment): bool
	{
		if (!$comment instanceof IdAwareInterface || !$comment->getId()) {
			return false;
		}
		
		$response = $this->client->put(
			$this->endPoint->getPullRequestCommentEndPoint($comment->getPullRequest()->getId(), $comment->getId()),
			[
				'json' => [
					"version" => 0,
					'text' => $comment->getMessage()
				]
			]
		);

		return in_array($response->getStatusCode(), [200, 201]);
	}
}
