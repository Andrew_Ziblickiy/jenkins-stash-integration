<?php

namespace Stash\PullRequest\Activity;

use Stash\User\UserAwareInterface;

/**
 * Interface ActivityInterface
 * @package Stash\PullRequest\Activity
 */
interface ActivityInterface extends UserAwareInterface
{
	/**
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * @return ActionInterface
	 */
	public function getAction() : ActionInterface;
}
