<?php

namespace Stash\PullRequest\Activity;

/**
 * Interface ActionInterface
 * @package Stash\PullRequest\Activity
 */
interface ActionInterface
{
	const COMMENTED = "COMMENTED";
	const RESCOPED = "RESCOPED";
	const MERGED = "MERGED";
	
	/**
	 * @return string
	 */
	public function getName() : string;
}
