<?php

namespace Stash\PullRequest\Activity\Action;

use Stash\PullRequest\Activity\ActionInterface;

/**
 * Class RescopedInterface
 * @package Stash\PullRequest\Activity\Action
 */
interface RescopedInterface extends ActionInterface
{
	/**
	 * @return string
	 */
	public function getFromHash() : string;
	
	/**
	 * @return string
	 */
	public function getToHash() : string;
}
