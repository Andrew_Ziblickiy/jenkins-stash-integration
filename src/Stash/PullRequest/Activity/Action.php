<?php

namespace Stash\PullRequest\Activity;

/**
 * Class Action
 * @package Stash\PullRequest\Activity
 */
class Action implements ActionInterface
{
	/**
	 * @var string
	 */
	protected $action;
	
	/**
	 * Action constructor.
	 * @param string $action
	 */
	public function __construct(string $action)
	{
		$this->action = $action;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string
	{
		return $this->action;
	}
	
	/**
	 * @return string
	 */
	public function __toString()
	{
		return $this->getName();
	}
}
