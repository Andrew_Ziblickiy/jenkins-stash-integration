<?php

namespace Stash\PullRequest;


use Stash\User\UserAwareTrait;

/**
 * Class Author
 * @package Stash\PullRequest
 */
class Author implements AuthorInterface
{
	use UserAwareTrait;
	
	/**
	 * @var string
	 */
	protected $role;
	
	/**
	 * @var bool
	 */
	protected $approved;
	
	/**
	 * Author constructor.
	 * @param string $role
	 * @param bool $approved
	 */
	public function __construct(string $role, bool $approved)
	{
		$this->role = $role;
		$this->approved = $approved;
	}
	
	/**
	 * @return string
	 */
	public function getRole(): string
	{
		return $this->role;
	}
	
	/**
	 * @return bool
	 */
	public function isApproved(): bool
	{
		return $this->approved;
	}
}