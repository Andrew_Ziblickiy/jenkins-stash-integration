<?php

namespace Stash\PullRequest;

/**
 * Interface MergeValidatorInterface
 * @package Stash\PullRequest
 */
interface MergeValidatorInterface
{
    /**
     * @param PullRequestInterface $pullRequest
     * @return bool
     */
    public function isPullRequestCanBeMerged(PullRequestInterface $pullRequest) : bool;
}
