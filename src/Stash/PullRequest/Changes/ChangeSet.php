<?php

namespace Stash\PullRequest\Changes;

/**
 * Class ChangeSet
 * @package Stash\PullRequest\Changes
 */
class ChangeSet implements ChangeSetInterface
{
    /**
     * @var ChangeInterface[]
     */
    protected $changes = [];

    /**
     * @param ChangeInterface $change
     */
    public function add(ChangeInterface $change)
    {
        $this->changes[] = $change;
    }

    /**
     * @return array
     */
    public function getChanges(): array
    {
        return $this->changes;
    }

    /**
     * @return bool
     */
    public function hasChanges(): bool
    {
        return count($this->changes) > 0;
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->getChanges());
    }

    /**
     * @return int|void
     */
    public function count()
    {
        return count($this->changes);
    }
}
