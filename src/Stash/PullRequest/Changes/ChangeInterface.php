<?php

namespace Stash\PullRequest\Changes;

/**
 * Interface ChangeInterface
 * @package Stash\PullRequest\Changes
 */
interface ChangeInterface
{
    /**
     * @return string
     */
    public function getPath() : string;

    /**
     * @return bool
     */
    public function isFile() : bool;

    /**
     * @return ChangeTypeInterface
     */
    public function getChangeType() : ChangeTypeInterface;
}
