<?php

namespace Stash\PullRequest\Changes;

interface ChangeTypeInterface
{
    /**
     * @return bool
     */
    public function isModified() : bool;

    /**
     * @return bool
     */
    public function isAdded() : bool;

    /**
     * @return bool
     */
    public function isCopied() : bool;

    /**
     * @return bool
     */
    public function isDeleted() : bool;

    /**
     * @return string
     */
    public function getType() : string;
}
