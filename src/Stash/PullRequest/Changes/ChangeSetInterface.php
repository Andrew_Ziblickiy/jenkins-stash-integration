<?php

namespace Stash\PullRequest\Changes;

/**
 * Class ChangeSetInterface
 * @package Stash\PullRequest\Changes
 */
interface ChangeSetInterface extends \Countable, \IteratorAggregate
{
    /**
     * @param ChangeInterface $change
     * @return void
     */
    public function add(ChangeInterface $change);

    /**
     * @return ChangeInterface[]
     */
    public function getChanges() : array;

    /**
     * @return bool
     */
    public function hasChanges() : bool;
}
