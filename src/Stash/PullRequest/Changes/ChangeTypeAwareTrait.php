<?php

namespace Stash\PullRequest\Changes;

trait ChangeTypeAwareTrait
{
    /**
     * @var ChangeTypeInterface
     */
    private $changeType;

    /**
     * @return ChangeTypeInterface
     */
    public function getChangeType() : ChangeTypeInterface
    {
        if (null === $this->changeType) {
            $this->changeType = new ChangeType();
        }

        return $this->changeType;
    }

    /**
     * @param ChangeTypeInterface $changeType
     * @return ChangeTypeAwareInterface|$this
     */
    public function setChangeType(ChangeTypeInterface $changeType) : ChangeTypeAwareInterface
    {
        $this->changeType = $changeType;

        return $this;
    }
}
