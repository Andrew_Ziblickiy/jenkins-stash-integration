<?php

namespace Stash\PullRequest\Changes;

/**
 * Class ChangeType
 * @package Stash\PullRequest\Changes
 */
class ChangeType implements ChangeTypeInterface
{
    /**
     * @var array
     */
    private $changes;

    /**
     * ChangeType constructor.
     * @param array $changes
     */
    public function __construct(array $changes = [])
    {
        $this->changes = $changes;
    }

    /**
     * @return bool
     */
    public function isModified(): bool
    {
        return $this->is('MODIFY');
    }

    /**
     * @return bool
     */
    public function isAdded(): bool
    {
        return $this->is('ADD');
    }

    /**
     * @return bool
     */
    public function isCopied(): bool
    {
        return $this->is('COPY');
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->is('DELETE');
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->changes['properties']['gitChangeType'] ?? '';
    }

    /**
     * @param string $type
     * @return bool
     */
    private function is(string $type) : bool
    {
        return $this->getType() == $type;
    }
}
