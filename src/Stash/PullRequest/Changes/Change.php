<?php

namespace Stash\PullRequest\Changes;

/**
 * Class Change
 * @package Stash\PullRequest\Changes
 */
class Change implements ChangeInterface, ChangeTypeAwareInterface
{
    use ChangeTypeAwareTrait;

    /**
     * @var array
     */
    protected $data;

    /**
     * Change constructor.
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->data['path']['toString'] ?? "";
    }

    /**
     * @return bool
     */
    public function isFile(): bool
    {
        return isset($this->data['nodeType']) ? $this->data['nodeType'] == 'FILE' : false;
    }
}
