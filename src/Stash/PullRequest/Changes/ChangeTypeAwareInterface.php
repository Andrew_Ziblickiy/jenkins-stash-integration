<?php

namespace Stash\PullRequest\Changes;

interface ChangeTypeAwareInterface
{
    /**
     * @return ChangeTypeInterface
     */
    public function getChangeType() : ChangeTypeInterface;

    /**
     * @param ChangeTypeInterface $changeType
     * @return ChangeTypeAwareInterface|$this
     */
    public function setChangeType(ChangeTypeInterface $changeType) : ChangeTypeAwareInterface;
}
