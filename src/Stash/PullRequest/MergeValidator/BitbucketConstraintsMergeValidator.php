<?php

namespace Stash\PullRequest\MergeValidator;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Stash\ClientAwareInterface;
use Stash\ClientAwareTrait;
use Stash\PullRequest\EndPointAwareInterface;
use Stash\PullRequest\EndPointAwareTrait;
use Stash\PullRequest\MergeValidatorInterface;
use Stash\PullRequest\PullRequestInterface;

/**
 * Class BitbucketConstraintsMergeValidator
 * @package Stash\PullRequest\MergeValidator
 */
class BitbucketConstraintsMergeValidator implements MergeValidatorInterface, ClientAwareInterface, EndPointAwareInterface
{
    use ClientAwareTrait;
    use EndPointAwareTrait;

    /**
     * @param PullRequestInterface $pullRequest
     * @return bool
     */
    public function isPullRequestCanBeMerged(PullRequestInterface $pullRequest): bool
    {
        try {
            $response = $this->requestRemoteMergeChecks($pullRequest);
        } catch (GuzzleException $e) {
            //  handle error
            return false;
        }

        $mergeChecks = $this->extractMergeChecksContent($response);

        return $this->validateMergeChecks($mergeChecks);
    }

    /**
     * @param PullRequestInterface $pullRequest
     * @return ResponseInterface
     * @throws GuzzleException
     */
    protected function requestRemoteMergeChecks(PullRequestInterface $pullRequest) : ResponseInterface
    {
        return $this->getClient()->request(
            'GET',
            $this->getEndPoint()->getMergeEndPoint($pullRequest->getId())
        );
    }

    /**
     * @param ResponseInterface $response
     * @return array
     */
    protected function extractMergeChecksContent(ResponseInterface $response) : array
    {
        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param array $mergeChecks
     * @return bool
     */
    protected function validateMergeChecks(array $mergeChecks) : bool
    {
        return (bool) $mergeChecks['canMerge'] ?? false;
    }
}
