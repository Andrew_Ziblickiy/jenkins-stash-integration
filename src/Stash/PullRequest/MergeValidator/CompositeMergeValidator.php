<?php

namespace Stash\PullRequest\MergeValidator;

use Stash\ClientAwareInterface;
use Stash\ClientAwareTrait;
use Stash\PullRequest\EndPointAwareInterface;
use Stash\PullRequest\EndPointAwareTrait;
use Stash\PullRequest\MergeValidatorInterface;
use Stash\PullRequest\PullRequestInterface;

/**
 * Class CompositeMergeValidator
 * @package Stash\PullRequest\MergeValidator
 */
class CompositeMergeValidator implements MergeValidatorInterface, ClientAwareInterface, EndPointAwareInterface
{
    use ClientAwareTrait;
    use EndPointAwareTrait;

    /**
     * @var MergeValidatorInterface[]
     */
    protected $validators = [];

    /**
     * @param MergeValidatorInterface $mergeValidator
     *
     * @return $this
     */
    public function addMergeValidator(MergeValidatorInterface $mergeValidator)
    {
        $this->validators[] = $mergeValidator;

        if ($mergeValidator instanceof ClientAwareInterface) {

            $client = $this->getClient();

            if ($client) {
                $mergeValidator->setClient($client);
            }
        }

        if ($mergeValidator instanceof EndPointAwareInterface) {
            $endPoint = $this->getEndPoint();

            if ($endPoint) {
                $mergeValidator->setEndPoint($this->getEndPoint());
            }
        }


        return $this;
    }

    /**
     * @param PullRequestInterface $pullRequest
     * @return bool
     */
    public function isPullRequestCanBeMerged(PullRequestInterface $pullRequest): bool
    {
        foreach ($this->validators as $validator) {
            if (!$validator->isPullRequestCanBeMerged($pullRequest)) {
                return false;
            }
        }

        return true;
    }
}
