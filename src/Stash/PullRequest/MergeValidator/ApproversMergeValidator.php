<?php

namespace Stash\PullRequest\MergeValidator;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Http\Message\ResponseInterface;
use Stash\ClientAwareInterface;
use Stash\ClientAwareTrait;
use Stash\PullRequest\EndPointAwareInterface;
use Stash\PullRequest\EndPointAwareTrait;
use Stash\PullRequest\MergeValidatorInterface;
use Stash\PullRequest\PullRequestInterface;

/**
 * Class ApproversMergeValidator
 * @package Stash\PullRequest\MergeValidator
 */
class ApproversMergeValidator implements MergeValidatorInterface, ClientAwareInterface, EndPointAwareInterface
{
    use ClientAwareTrait;
    use EndPointAwareTrait;

    protected $approvers;

    /**
     * ApproversMergeValidator constructor.
     * @param array $approvers
     */
    public function __construct(array $approvers = [])
    {
        $this->approvers = $approvers;
    }

    /**
     * @param PullRequestInterface $pullRequest
     * @throws GuzzleException
     * @return bool
     */
    public function isPullRequestCanBeMerged(PullRequestInterface $pullRequest): bool
    {
        $response = $this->requestParticipants($pullRequest);
        $participants = $this->extractDataFromResponse($response);

        if (!$this->isImportantParticipantInApproverList($participants, $this->approvers)) {
            return false;
        }

        return $this->processParticipants($participants);
    }

    /**
     * @param array $participants
     * @param array $importantApproverNames
     *
     * @return bool
     */
    protected function isImportantParticipantInApproverList(array $participants, array $importantApproverNames) : bool
    {
        //  get only reviewers, in case important guy is owner of pr
        $participantNames = $this->extractParticipantNames(
            $this->extractReviewers($participants)
        );

        //  unset jenkins
        if (in_array('nlsvc-jenkins-docker', $importantApproverNames)) {
            unset($importantApproverNames[array_search('nlsvc-jenkins-docker', $importantApproverNames)]);
        }

        //  check if important approver exists in pr, else decline merging
        if (!array_intersect($participantNames, $importantApproverNames)) {
            return false;
        }

        return true;
    }

    /**
     * @param array $participants
     * @return array
     */
    protected function extractReviewers(array $participants)
    {
        return array_filter($participants['values'], function ($participant) {
            return $this->isParticipantReviewer($participant);
        });
    }

    /**
     * @param array $participants
     * @return array
     */
    protected function extractParticipantNames(array  $participants)
    {
        return array_map(function ($item) {
            return $item['user']['name'];
        }, $participants);
    }

    /**
     * @param PullRequestInterface $pullRequest
     * @return ResponseInterface
     * @throws GuzzleException
     */
    protected function requestParticipants(PullRequestInterface $pullRequest) : ResponseInterface
    {
        return $this->getClient()->request(
            'GET',
            $this->getEndPoint()->getParticipants($pullRequest->getId())
        );
    }

    /**
     * @param ResponseInterface $response
     * @return array
     */
    protected function extractDataFromResponse(ResponseInterface $response) : array
    {
        return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @param array $records
     * @return bool
     */
    protected function processParticipants(array $records) : bool
    {
        foreach ($records['values'] as $item) {
            if (!$this->validateItem($item)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param array $item
     *
     * @return bool
     */
    protected function validateItem(array $item)
    {
        if ($this->isParticipantAuthor($item)) {
            return true;
        }

        if ($this->isApproved($item)) {
            //  be aware, that if participant approved pr then okay, but if not then we check if participant vote is important
            //  in next if statement
            //  that means that this block should be here
            return true;
        }

        return false;
    }

    /**
     * @param array $record
     * @return bool
     */
    protected function isParticipantImportant(array $record) : bool
    {
        return in_array($record['user']['name'], $this->approvers);
    }

    /**
     * @param array $record
     * @return bool
     */
    protected function isApproved(array $record) : bool
    {
        return $record['status'] == 'APPROVED';
    }

    /**
     * @param array $record
     * @return bool
     */
    protected function isParticipantAuthor(array $record) : bool
    {
        return $record['role'] == 'AUTHOR';
    }

    /**
     * @param array $record
     * @return bool
     */
    protected function isParticipantReviewer(array $record) : bool
    {
        return $record['role'] == 'REVIEWER';
    }
}
