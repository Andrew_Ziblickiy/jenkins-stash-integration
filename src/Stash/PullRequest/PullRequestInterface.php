<?php

namespace Stash\PullRequest;

use Stash\User\UserInterface;

/**
 * Interface PullRequestInterface
 *
 * @url https://developer.atlassian.com/static/rest/stash/3.11.3/stash-rest.html#idp157520
 * @package Stash\PullRequest
 */
interface PullRequestInterface
{
	const STATE_OPEN = 'open';
	
	/**
	 * @return int
	 */
	public function getId() : int;
	
	/**
	 * @return string
	 */
	public function getTitle() : string;
	
	/**
	 * @return string
	 */
	public function getState() : string;
	
	/**
	 * @return bool
	 */
	public function isOpen() : bool;
	
	/**
	 * @return bool
	 */
	public function isClosed() : bool;

    /**
     * @return int
     */
	public function getVersion() : int;
	
	/**
	 * @return RefInterface
	 */
	public function getFromRef() : RefInterface;
	
	/**
	 * @return RefInterface
	 */
	public function getToRef() : RefInterface;
	
	/**
	 * @return AuthorInterface
	 */
	public function getAuthor() : AuthorInterface;
	
	/**
	 * @return \DateTime
	 */
	public function getCreatedDate() : \DateTime;
	
	/**
	 * @return \DateTime
	 */
	public function getUpdatedDate() : \DateTime;
}
