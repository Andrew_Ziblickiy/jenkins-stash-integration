<?php

namespace Stash\PullRequest;

use Stash\Property\NotPassedInterface;
use Stash\User\UserAwareTrait;

/**
 * Class UndefinedPullRequest
 * @package Stash\PullRequest
 */
class UndefinedPullRequest implements PullRequestInterface, NotPassedInterface
{
	/**
	 * @var RefInterface
	 */
	protected $ref;
	
	/**
	 * @var AuthorInterface
	 */
	protected $author;
	
	/**
	 * @var \DateTime
	 */
	protected $date;
	
	/**
	 * @var int
	 */
	protected $id;
	
	public function __construct($id = 0)
	{
		$this->id = $id;
		$this->ref = new class implements RefInterface, NotPassedInterface {
			
			public function getId(): string
			{
				return '';
			}
			
			public function getDisplayId(): string
			{
				return '';
			}
			
			public function getLatestChangeset(): string
			{
				return '';
			}
		};
		
		$this->author = new class implements AuthorInterface, NotPassedInterface {
			
			use UserAwareTrait;
			
			/**
			 * @return string
			 */
			public function getRole(): string
			{
				return 'Undefined';
			}
			
			/**
			 * @return bool
			 */
			public function isApproved(): bool
			{
				return false;
			}
		};
		
		$this->date = new \DateTime();
	}
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return 'Undefined';
	}
	
	/**
	 * @return string
	 */
	public function getState(): string
	{
		return 'Unknown';
	}
	
	/**
	 * @return bool
	 */
	public function isOpen(): bool
	{
		return false;
	}
	
	/**
	 * @return bool
	 */
	public function isClosed(): bool
	{
		return false;
	}

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return -1;
    }

    /**
	 * @return RefInterface
	 */
	public function getFromRef(): RefInterface
	{
		return $this->ref;
	}
	
	/**
	 * @return RefInterface
	 */
	public function getToRef(): RefInterface
	{
		return $this->ref;
	}
	
	/**
	 * @return AuthorInterface
	 */
	public function getAuthor(): AuthorInterface
	{
		return $this->author;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getCreatedDate(): \DateTime
	{
		return $this->date;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getUpdatedDate(): \DateTime
	{
		return $this->date;
	}
}
