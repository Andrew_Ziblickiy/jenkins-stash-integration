<?php

namespace Stash\PullRequest;

use Stash\User\UserInterface;

/**
 * Class PullRequest
 * @package Stash\PullRequest
 */
class PullRequest implements PullRequestInterface
{
	/**
	 * @var int
	 */
	protected $id;
	
	/**
	 * @var string
	 */
	protected $title;
	
	/**
	 * @var string
	 */
	protected $state;
	
	/**
	 * @var bool
	 */
	protected $open = true;
	
	/**
	 * @var bool
	 */
	protected $closed = false;

    /**
     * @var int
     */
	protected $version = -1;
	
	/**
	 * @var RefInterface
	 */
	protected $fromRef;
	
	/**
	 * @var RefInterface
	 */
	protected $toRef;
	
	/**
	 * @var AuthorInterface
	 */
	protected $author;
	
	/**
	 * @var \DateTime
	 */
	protected $createdDate;
	
	/**
	 * @var \DateTime
	 */
	protected $updatedDate;
	
	/**
	 * PullRequest constructor.
	 * @param int $id
	 * @param string $title
	 * @param string $state
	 */
	public function __construct(int $id, string $title, string $state)
	{
		$this->id = $id;
		$this->title = $title;
		$this->state = $state;
	}
	
	/**
	 * @return int
	 */
	public function getId(): int
	{
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getTitle(): string
	{
		return $this->title;
	}
	
	/**
	 * @return string
	 */
	public function getState(): string
	{
		return $this->state;
	}
	
	/**
	 * @return bool
	 */
	public function isOpen(): bool
	{
		return $this->open;
	}
	
	/**
	 * @param bool $open
	 */
	public function setOpen(bool $open)
	{
		$this->open = $open;
	}
	
	/**
	 * @return bool
	 */
	public function isClosed(): bool
	{
		return $this->closed;
	}
	
	/**
	 * @param bool $closed
	 */
	public function setClosed(bool $closed)
	{
		$this->closed = $closed;
	}

    /**
     * @return int
     */
    public function getVersion(): int
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion(int $version)
    {
        $this->version = $version;
    }
	
	/**
	 * @return RefInterface
	 */
	public function getFromRef(): RefInterface
	{
		return $this->fromRef;
	}
	
	/**
	 * @param RefInterface $fromRef
	 */
	public function setFromRef(RefInterface $fromRef)
	{
		$this->fromRef = $fromRef;
	}
	
	/**
	 * @return RefInterface
	 */
	public function getToRef(): RefInterface
	{
		return $this->toRef;
	}
	
	/**
	 * @param RefInterface $toRef
	 */
	public function setToRef(RefInterface $toRef)
	{
		$this->toRef = $toRef;
	}
	
	/**
	 * @return AuthorInterface
	 */
	public function getAuthor(): AuthorInterface
	{
		return $this->author;
	}
	
	/**
	 * @param AuthorInterface $author
	 * @return $this
	 */
	public function setAuthor(AuthorInterface $author)
	{
		$this->author = $author;
		
		return $this;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getCreatedDate(): \DateTime
	{
		return $this->createdDate;
	}
	
	/**
	 * @return \DateTime
	 */
	public function getUpdatedDate(): \DateTime
	{
		return $this->updatedDate;
	}
	
	/**
	 * @param \DateTime $createdDate
	 */
	public function setCreatedDate(\DateTime $createdDate)
	{
		$this->createdDate = $createdDate;
	}
	
	/**
	 * @param \DateTime $updatedDate
	 */
	public function setUpdatedDate(\DateTime $updatedDate)
	{
		$this->updatedDate = $updatedDate;
	}
}
