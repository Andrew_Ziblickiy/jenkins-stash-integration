<?php

namespace Stash\PullRequest\DataMapper;

use Stash\DataMapper\DataMapperInterface;
use Stash\PullRequest\Author;
use Stash\PullRequest\PullRequest;
use Stash\PullRequest\PullRequestInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * Class PullRequestDataMapper
 * @package Stash\PullRequest\DataMapper
 */
class PullRequestDataMapper implements DataMapperInterface
{
	/**
	 * @var \Symfony\Component\PropertyAccess\PropertyAccessor
	 */
	protected $propertyAccessor;
	
	/**
	 * @var DataMapperInterface
	 */
	protected $userMapper;
	
	/**
	 * @var DataMapperInterface
	 */
	protected $refMapper;
	
	/**
	 * PullRequestDataMapper constructor.
	 * @param DataMapperInterface $userMapper
	 * @param DataMapperInterface $refMapper
	 */
	public function __construct(DataMapperInterface $userMapper, DataMapperInterface $refMapper)
	{
		$this->propertyAccessor = PropertyAccess::createPropertyAccessor();
		$this->userMapper = $userMapper;
		$this->refMapper = $refMapper;
	}
	
	/**
	 * @param string $className
	 * @return bool
	 */
	public function canMap(string $className): bool
	{
		return PullRequestInterface::class == $className;
	}
	
	/**
	 * @param array $data
	 *
	 * @return PullRequestInterface
	 */
	public function map(array $data)
	{
		//  todo can be improved
		$pr = new PullRequest((int) $data['id'], $data['title'], $data['state']);
		$pr->setOpen(boolval($data['open']));
		$pr->setClosed(boolval($data['closed']));
		$pr->setFromRef($this->refMapper->map($data['fromRef']));
		$pr->setToRef($this->refMapper->map($data['toRef']));
		
		$author = new Author($data['author']['role'], boolval($data['author']['approved']));
		$author->setUser($this->userMapper->map($data['author']['user']));
		$pr->setAuthor($author);

		$pr->setVersion((int) $data['version']);
//		$pr->setCreatedDate(new \DateTime($data['createdDate']));
//		$pr->setUpdatedDate(new \DateTime($data['updatedDate']));
		return $pr;
	}
}
