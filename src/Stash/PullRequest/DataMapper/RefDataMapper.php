<?php

namespace Stash\PullRequest\DataMapper;

use Stash\DataMapper\DataMapperInterface;
use Stash\PullRequest\Ref;
use Stash\PullRequest\RefInterface;

/**
 * Class RefDataMapper
 * @package Stash\PullRequest\DataMapper
 */
class RefDataMapper implements DataMapperInterface
{
	/**
	 * @param string $className
	 * @return bool
	 */
	public function canMap(string $className): bool
	{
		return $className === RefInterface::class;
	}
	
	/**
	 * @param array $data
	 *
	 * @return RefInterface
	 */
	public function map(array $data)
	{
		return new Ref($data['id'], $data['displayId'], $data['latestCommit']);
	}
}
