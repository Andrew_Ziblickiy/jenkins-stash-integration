<?php

namespace Stash\PullRequest;

use Stash\PullRequest\Changes\ChangeSetInterface;
use Stash\PullRequest\Comment\CommentInterface;

/**
 * Class PullRequestManagerInterface
 * @package Stash\PullRequest
 */
interface PullRequestManagerInterface
{
	/**
	 * @param PullRequestInterface $pullRequest
	 * @return bool
	 */
	public function approve(PullRequestInterface $pullRequest) : bool;
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @return bool
	 */
	public function unApprove(PullRequestInterface $pullRequest) : bool;

    /**
     * @param PullRequestInterface $pullRequest
     * @return ChangeSetInterface
     */
	public function getChangeSet(PullRequestInterface $pullRequest) : ChangeSetInterface;

    /**
     * @param PullRequestInterface $pullRequest
     * @return bool
     */
	public function merge(PullRequestInterface $pullRequest) : bool;
	
	/**
	 * @param CommentInterface $comment
	 * @return bool
	 */
	public function addComment(CommentInterface $comment) : bool;
	
	/**
	 * @param CommentInterface $comment
	 * @return bool
	 */
	public function updateCommend(CommentInterface $comment) : bool;
}
