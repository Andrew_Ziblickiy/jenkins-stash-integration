<?php

namespace Stash\PullRequest;

/**
 * Trait PullRequestAwareTrait
 * @package Stash\PullRequest
 */
trait PullRequestAwareTrait
{
	/**
	 * @var PullRequestInterface
	 */
	protected $pullRequest;
	
	/**
	 * @return PullRequestInterface
	 */
	public function getPullRequest() : PullRequestInterface
	{
		if (null === $this->pullRequest) {
			$this->pullRequest = new UndefinedPullRequest();
		}
		
		return $this->pullRequest;
	}
	
	/**
	 * @param PullRequestInterface $pullRequest
	 * @return $this
	 */
	public function setPullRequest(PullRequestInterface $pullRequest)
	{
		$this->pullRequest = $pullRequest;
		
		return $this;
	}
}
