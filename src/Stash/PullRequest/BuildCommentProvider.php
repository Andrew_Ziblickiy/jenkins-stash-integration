<?php

namespace Stash\PullRequest;

use Stash\PullRequest\Comment\Comment;
use Stash\PullRequest\Comment\CommentInterface;
use Stash\PullRequest\Jenkins\BuildInterface;

/**
 * Class BuildCommentProvider
 * @package Stash\PullRequest
 */
class BuildCommentProvider implements BuildCommentProviderInterface
{
    /**
     * @var string
     */
    protected $templateFile;

    /**
     * BuildCommentProvider constructor.
     * @param string $resourceRoot
     */
    public function __construct(string $resourceRoot)
    {
        $this->templateFile = $resourceRoot . DIRECTORY_SEPARATOR . 'comment.message.template';
    }

    /**
     * @param BuildInterface $build
     * @return CommentInterface
     */
    public function getBuildComment(BuildInterface $build): CommentInterface
    {
        $message = $this->parseMessage($this->getMessageTemplate(), $build->getParameters()->all());
        return new Comment(
            new UndefinedPullRequest($build->getPullRequestId()),
            $message
        );
    }


    /**
     * @param string $message
     * @param array $variables
     * @return string
     */
    protected function parseMessage(string $message, array $variables = []) : string
    {
        $keys = array_keys($variables);
        $values = array_values($variables);
        return str_replace(
            array_map(function ($value) {
                return '${' . $value . '}';
            }, $keys),
            $values,
            $message
        );
    }

    protected function getMessageTemplate() : string
    {
        return file_get_contents($this->templateFile);
    }
}
