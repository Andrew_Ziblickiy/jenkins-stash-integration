<?php

namespace Stash\PullRequest;

/**
 * Interface RefInterface
 *
 * @url https://developer.atlassian.com/static/rest/stash/3.11.3/stash-rest.html#idp157520
 * @package Stash\PullRequest
 */
interface RefInterface
{
	/**
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * @return string
	 */
	public function getDisplayId() : string;
	
	/**
	 * @return string
	 */
	public function getLatestChangeset() : string;
}
