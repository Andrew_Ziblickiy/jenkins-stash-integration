<?php

namespace Stash\PullRequest;

/**
 * Class Ref
 * @package Stash\PullRequest
 */
class Ref implements RefInterface
{
	/**
	 * @var string
	 */
	protected $id;
	
	/**
	 * @var string
	 */
	protected $displayId;
	
	/**
	 * @var string
	 */
	protected $latestChangeset;
	
	/**
	 * Ref constructor.
	 * @param string $id
	 * @param string $displayId
	 * @param string $latestChangeset
	 */
	public function __construct(string $id, string $displayId, string $latestChangeset)
	{
		$this->id = $id;
		$this->displayId = $displayId;
		$this->latestChangeset = $latestChangeset;
	}
	
	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}
	
	/**
	 * @return string
	 */
	public function getDisplayId(): string
	{
		return $this->displayId;
	}
	
	/**
	 * @return string
	 */
	public function getLatestChangeset(): string
	{
		return $this->latestChangeset;
	}
}
