<?php

namespace Stash\PullRequest\Jenkins;

/**
 * Class BuildCollection
 * @package Stash\PullRequest\Jenkins
 */
class BuildCollection implements BuildCollectionInterface
{
	/**
	 * @var BuildInterface[]
	 */
	protected $collection = [];

	protected $keyPullRequestIdAssociationMap = [];
	
	/**
	 * @param BuildInterface $build
	 * @return BuildCollectionInterface
	 */
	public function addBuild(BuildInterface $build): BuildCollectionInterface
	{
		$this->keyPullRequestIdAssociationMap[$build->getPullRequestId()] = array_push($this->collection, $build) - 1;
		return $this;
	}

	/**
	 * @param BuildInterface $build
	 */
	public function removeBuild(BuildInterface $build)
	{
		if (isset($this->keyPullRequestIdAssociationMap[$build->getPullRequestId()])) {
			unset($this->collection[$this->keyPullRequestIdAssociationMap[$build->getPullRequestId()]]);
		}
	}


	/**
	 * @return BuildInterface[]
	 */
	public function getBuilds()
	{
		return $this->collection;
	}
}
