<?php

namespace Stash\PullRequest\Jenkins\EventListener;

use Stash\Commit\BuildStatus\BuildStatusManagerInterface;
use Stash\DataProvider\DataProviderInterface;
use Stash\PullRequest\Command\Jenkins\Event\BuildStatusEvent;
use Stash\PullRequest\Jenkins\BuildInterface;
use Stash\PullRequest\PullRequestManagerInterface;

/**
 * Class BuildStatusEventListener
 * @package Stash\PullRequest\Jenkins\EventListener
 */
class BuildStatusEventListener
{
	/**
	 * @var PullRequestManagerInterface
	 */
	protected $pullRequestManager;

	/**
	 * @var BuildStatusManagerInterface
	 */
	protected $buildStatusManager;

	/**
	 * @var DataProviderInterface
	 */
	protected $pullRequestDataProvider;

	/**
	 * JobStartEventListener constructor.
	 * @param PullRequestManagerInterface $manager
	 * @param BuildStatusManagerInterface $buildStatusManager
	 * @param DataProviderInterface $pullRequestDataProvider
	 */
	public function __construct(PullRequestManagerInterface $manager, BuildStatusManagerInterface $buildStatusManager, DataProviderInterface $pullRequestDataProvider)
	{
		//  todo refactor buildStatusManager class name
		$this->pullRequestManager = $manager;
		$this->buildStatusManager = $buildStatusManager;
		$this->pullRequestDataProvider = $pullRequestDataProvider;
	}

	/**
	 * @param BuildStatusEvent $event
	 */
	public function handle(BuildStatusEvent $event)
	{
		$build = $event->getBuild();

		if ($event->isError()) {
			$this->handleFail($build);
		}
	}

	protected function handleFail(BuildInterface $build)
	{

	}
}
