<?php

namespace Stash\PullRequest\Jenkins\EventListener;

use Stash\Commit\BuildStatus\BuildStatus;
use Stash\Commit\BuildStatus\BuildStatusAwareInterface;
use Stash\Commit\BuildStatus\BuildStatusInterface;
use Stash\Commit\BuildStatus\BuildStatusManagerInterface;
use Stash\Commit\Commit;
use Stash\PullRequest\BuildCommentProviderInterface;
use Stash\PullRequest\Command\Jenkins\Event\JobActionEvent;
use Stash\PullRequest\Comment\Comment;
use Stash\PullRequest\Jenkins\BuildInterface;
use Stash\PullRequest\PullRequestAwareInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Stash\PullRequest\UndefinedPullRequest;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Class JobStartEventListener
 * @package Stash\PullRequest\Jenkins\EventListener
 */
class JobStartEventListener
{
	/**
	 * @var PullRequestManagerInterface
	 */
	protected $pullRequestManager;

	/**
	 * @var BuildStatusManagerInterface
	 */
	protected $buildStatusManager;

    /**
     * @var BuildCommentProviderInterface
     */
	protected $buildCommentProvider;

    /**
     * JobStartEventListener constructor.
     * @param PullRequestManagerInterface $manager
     * @param BuildStatusManagerInterface $buildStatusManager
     * @param BuildCommentProviderInterface $buildCommentProvider
     */
	public function __construct(
	    PullRequestManagerInterface $manager,
        BuildStatusManagerInterface $buildStatusManager,
        BuildCommentProviderInterface $buildCommentProvider
    ) {
		//  todo refactor buildStatusManager class name
		$this->pullRequestManager = $manager;
		$this->buildStatusManager = $buildStatusManager;
		$this->buildCommentProvider = $buildCommentProvider;
	}

	/**
	 * @param JobActionEvent $event
	 */
	public function onStart(JobActionEvent $event)
	{
		$build = $event->getBuild();

		if ($build instanceof PullRequestAwareInterface) {
		    $pr = $build->getPullRequest();
		    if ($pr) {
		        //  change source commits
                $build->setSourceBranchCommit($pr->getFromRef()->getLatestChangeset());
                $build->setDestinationBranchCommit($pr->getToRef()->getLatestChangeset());
            }
        }

		$this->createBuildComment($build, $event->getInput());
		$this->createBuildStatus($build, $event->getInput());
	}

	/**
	 * @param BuildInterface $build
	 */
	protected function createBuildComment(BuildInterface $build, InputInterface $input)
	{
	    $comment = $this->buildCommentProvider->getBuildComment($build);

		if ($this->pullRequestManager->addComment($comment)) {
			if ($comment->getId()) {
				$build->setJobDescriptionCommentId($comment->getId());
			}
		}
	}

	/**
	 * @param BuildInterface $build
	 * @param InputInterface $input
	 */
	protected function createBuildStatus(BuildInterface $build, InputInterface $input)
	{
		$status = new BuildStatus(
			BuildStatusInterface::STATE_INPROGRESS,
			$build->getSourceBranch(),
			sprintf("%s-%s", $build->getSourceBranch(), $input->getOption('buildId')),
			sprintf("http://jenkins-docker.nl.corp.tele2.com/job/tele2-pr/%s/", $input->getOption('buildId')),
			""
		);
		$commit = new Commit($build->getSourceBranchCommit());

		if (
			$this->buildStatusManager->associateStatusWithCommit($status, $commit) &&
			$build instanceof BuildStatusAwareInterface
		) {
		    $status->setCommit($commit);
			$build->setBuildStatus($status);
		}
	}
}
