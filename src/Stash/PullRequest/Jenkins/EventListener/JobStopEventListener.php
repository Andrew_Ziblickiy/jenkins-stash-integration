<?php

namespace Stash\PullRequest\Jenkins\EventListener;

use Stash\Commit\BuildStatus\BuildStatusAwareInterface;
use Stash\Commit\BuildStatus\BuildStatusesAwareInterface;
use Stash\Commit\BuildStatus\BuildStatusInterface;
use Stash\Commit\BuildStatus\BuildStatusManagerInterface;
use Stash\Commit\Commit;
use Stash\Commit\CommitAwareInterface;
use Stash\PullRequest\Command\Jenkins\Event\JobActionEvent;
use Stash\PullRequest\Jenkins\BuildInterface;
use Stash\PullRequest\PullRequestAwareInterface;
use Stash\PullRequest\PullRequestManagerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class JobStopEventListener
 * @package Stash\PullRequest\Jenkins\EventListener
 */
class JobStopEventListener extends Event
{
	/**
	 * @var PullRequestManagerInterface
	 */
	protected $pullRequestManager;
	
	/**
	 * @var BuildStatusManagerInterface
	 */
	protected $buildStatusManager;
	
	/**
	 * JobStartEventListener constructor.
	 * @param PullRequestManagerInterface $manager
	 * @param BuildStatusManagerInterface $buildStatusManager
	 */
	public function __construct(PullRequestManagerInterface $manager, BuildStatusManagerInterface $buildStatusManager)
	{
		//  todo refactor buildStatusManager class name
		$this->pullRequestManager = $manager;
		$this->buildStatusManager = $buildStatusManager;
	}
	
	/**
	 * @param JobActionEvent $event
	 */
	public function onStop(JobActionEvent $event)
	{
		$build = $event->getBuild();
		$input = $event->getInput();

		$status = $input->getOption('success') ? true : !(bool) $input->getOption('error');

		if ($status) {
		    if ($build instanceof BuildStatusesAwareInterface) {
		        $this->markAsSuccessFailedBuildStatuses($build->getBuildStatuses());
            }
        }

		$this->updateBuildStatus($build, $status);

		$this->postMessage($event->getBuild(), $event->getInput());
	}

    /**
     * @param BuildInterface $build
     * @param InputInterface $input
     */
	protected function postMessage(BuildInterface $build, InputInterface $input)
    {
        //  todo move code below to event handler
        $failReason = "Not Specified";
        $failLog = "Has no log, sorry";
        $message = $input->getOption('message');
        $messageLogFile = $input->getOption('messageFile');

        if ($input->getOption('success')) {
            $build->setSuccess(true);
            $message =<<<MSG
[*BuildFinished*] [%s] %s into [%s] %s

**✓ Build Success**

MSG;
            $message = sprintf(
                $message,
                $build->getSourceBranch(),
                $build->getSourceBranchCommit(),
                $build->getDestinationBranch(),
                $build->getDestinationBranchCommit()
            );
        } else if ($input->getOption('error')) {
            $build->setSuccess(false);
            if ($message) {
                $failReason = $message;
            }
            if ($messageLogFile) {
                $failLog = "\n```\n";
                $failLog .= $this->tailFile($messageLogFile, $input->getOption('messageFileLength'), (bool) $input->getOption('messageFileFullLength'));
                $failLog .= "\n```";
            }

            $message =<<<MSG
[*BuildFinished*] [%s] %s into [%s] %s

**✕ Build Failed**

**Reason: $failReason**
MSG;
            $message = @sprintf(
                $message,
                $build->getSourceBranch(),
                $build->getSourceBranchCommit(),
                $build->getDestinationBranch(),
                $build->getDestinationBranchCommit()
            );
            $message .= "\n\n" . $failLog;
        }

        $manager = $this->getPullRequestManager();
        $comment = new Comment(
            new UndefinedPullRequest($build->getPullRequestId()),
            $message
        );

        if ($build->getJobDescriptionCommentId()) {
            $comment->setId($build->getJobDescriptionCommentId());
            $manager->updateCommend($comment);
        } else {
            $manager->addComment($comment);
        }
    }

    /**
     * @param BuildStatusInterface[] $buildStatuses
     */
	protected function markAsSuccessFailedBuildStatuses(array $buildStatuses)
    {
        foreach ($buildStatuses as $buildStatus) {
            if ($buildStatus instanceof CommitAwareInterface) {
                if ($buildStatus->getState() == BuildStatusInterface::STATE_FAILED) {
                    $buildStatus->setState(BuildStatusInterface::STATE_SUCCESSFUL);
                    $this->buildStatusManager->associateStatusWithCommit($buildStatus, $buildStatus->getCommit());
                }
            }
        }
    }
	
	/**
	 * @param BuildInterface $build
	 */
	protected function updateBuildStatus(BuildInterface $build, bool $isSuccess)
	{
		if ($build instanceof BuildStatusAwareInterface) {
			$status = $build->getBuildStatus();
			
			if ($status) {
				$status->setState($isSuccess ? BuildStatusInterface::STATE_SUCCESSFUL : BuildStatusInterface::STATE_FAILED);
				$state = $this->buildStatusManager->associateStatusWithCommit(
					$status,
					new Commit($build->getSourceBranchCommit())
				);

				if (!$state && $build instanceof PullRequestAwareInterface) {
					$this->buildStatusManager->associateStatusWithCommit(
						$status,
						new Commit($build->getPullRequest()->getFromRef()->getLatestChangeset())
					);
				}
			}
		}
	}
}
