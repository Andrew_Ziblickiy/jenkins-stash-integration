<?php

namespace Stash\PullRequest\Jenkins\Exception;

use Throwable;

/**
 * Class BuildNotFoundException
 * @package Stash\PullRequest\Jenkins\Exception
 */
class BuildNotFoundException extends \Exception
{
	/**
	 * BuildNotFoundException constructor.
	 * @param string $message
	 * @param int $code
	 * @param Throwable|null $previous
	 */
	public function __construct($message = "Build not found", $code = 1, Throwable $previous = null)
	{
		parent::__construct($message, $code, $previous);
	}
}
