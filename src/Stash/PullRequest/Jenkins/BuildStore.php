<?php

namespace Stash\PullRequest\Jenkins;

use Stash\PullRequest\Jenkins\Exception\BuildNotFoundException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Exception\IOException;

/**
 * Class BuildStore
 * @package Stash\PullRequest\Jenkins
 */
class BuildStore implements BuildStoreInterface
{
	/**
	 * @var string
	 */
	protected $filesystem;
	
	/**
	 * @var string
	 */
	protected $cacheRootDir;
	
	/**
	 * BuildStore constructor.
	 * @param Filesystem $filesystem
	 * @param string $cacheRootDir
	 */
	public function __construct(Filesystem $filesystem, string $cacheRootDir)
	{
		$this->filesystem = $filesystem;
		$this->cacheRootDir = $cacheRootDir;
		
		$cacheFolder = $this->getCacheFolder();
		$filesystem->mkdir($cacheFolder);
	}
	
	/**
	 * @param int|string $pullRequestId
	 * @return BuildInterface
	 * @throws BuildNotFoundException
	 */
	public function load($pullRequestId): BuildInterface
	{
		$cacheFile = $this->makeCacheFileName($pullRequestId);
		
		if (!file_exists($cacheFile)) {
			throw new BuildNotFoundException();
		}
		
		return unserialize(file_get_contents($cacheFile));
	}
	
	/**
	 * @param BuildInterface $build
	 * @return bool
	 */
	public function save(BuildInterface $build)
	{
		$cacheFile = $this->makeCacheFileName($build->getPullRequestId());
		try {
			$this->filesystem->dumpFile($cacheFile, serialize($build));
		} catch (IOException $exception) {
			// do nothing =) just return false
			return false;
		}
		
		return true;
	}
	
	/**
	 * @param $pullRequestId
	 * @return bool
	 */
	public function has($pullRequestId): bool
	{
		return file_exists($this->makeCacheFileName($pullRequestId));
	}

    /**
     * @param BuildCollectionInterface $collection
     * @return bool
     */
    public function cleanBuildsWhichIsNotInList(BuildCollectionInterface $collection)
    {
        $existed = [];

        foreach ($collection->getBuilds() as $build) {
            $existed[$this->makeCacheFileName($build->getPullRequestId())] = $build;
        }

        foreach (glob($this->makeCacheFileName('*')) as $file) {
            if (!isset($existed[$file])) {
                try {
                    $this->filesystem->remove($file);
//                    $collection->removeBuild($existed[$file]);
                } catch (IOException $e) {}
            }
        }

        return true;
    }

    /**
	 * @return string
	 */
	protected function getCacheFolder()
	{
		return rtrim($this->cacheRootDir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . 'pr';
	}
	
	/**
	 * @param string $id
	 * @return string
	 */
	protected function makeCacheFileName($id)
	{
		return sprintf(
			"%s%s%s.serialized",
			$this->getCacheFolder(),
			DIRECTORY_SEPARATOR,
			$id
		);
	}
}
