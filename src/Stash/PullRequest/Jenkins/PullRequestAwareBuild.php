<?php

namespace Stash\PullRequest\Jenkins;

use Stash\PullRequest\PullRequestAwareInterface;
use Stash\PullRequest\PullRequestAwareTrait;
use Stash\PullRequest\PullRequestInterface;

/**
 * Class PullRequestAwareBuild
 * @package Stash\PullRequest\Jenkins
 */
class PullRequestAwareBuild implements BuildAwareInterface, PullRequestAwareInterface
{
	use PullRequestAwareTrait;
	
	/**
	 * @var BuildInterface
	 */
	protected $build;
	
	/**
	 * @var PullRequestInterface
	 */
	protected $pullRequest;
	
	/**
	 * PullRequestAwareBuild constructor.
	 * @param BuildInterface $build
	 * @param PullRequestInterface $pullRequest
	 */
	public function __construct(BuildInterface $build, PullRequestInterface $pullRequest)
	{
		$this->setPullRequest($pullRequest);
		$this->build = $build;
	}
	
	/**
	 * @return BuildInterface
	 */
	public function getBuild(): BuildInterface
	{
		return $this->build;
	}
}
