<?php

namespace Stash\PullRequest\Jenkins;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Interface BuildInterface
 * @package Stash\PullRequest\Jenkins
 */
interface BuildInterface extends \Serializable
{
	/**
	 * @return int
	 */
	public function getPullRequestId() : int;
	
	/**
	 * @return string
	 */
	public function getSourceBranchCommit() : string;
	
	/**
	 * @param string $commit
	 * @return BuildInterface
	 */
	public function setSourceBranchCommit(string $commit) : BuildInterface;
	
	/**
	 * @return string
	 */
	public function getSourceBranch() : string;
	
	/**
	 * @return string
	 */
	public function getDestinationBranch() : string;
	
	/**
	 * @return string
	 */
	public function getDestinationBranchCommit() : string;
	
	/**
	 * @param string $commit
	 * @return BuildInterface
	 */
	public function setDestinationBranchCommit(string $commit) : BuildInterface;
	
	/**
	 * @return int
	 */
	public function getBuildTimestamp() : int;
	
	/**
	 * @param int $timestamp
	 * @return $this
	 */
	public function setBuildTimestamp(int $timestamp);
	
	/**
	 * @return bool
	 */
	public function isInProgress() : bool;
	
	/**
	 * @return bool
	 */
	public function isCompleted() : bool;
	
	/**
	 * @return bool
	 */
	public function isBuilt() : bool;
	
	/**
	 * @return bool
	 */
	public function isSuccess() : bool;
	
	/**
	 * Mark build as started
	 *
	 * @return $this
	 */
	public function start() : BuildInterface;
	
	/**
	 * Mark build as completed
	 *
	 * @return BuildInterface
	 */
	public function stop() : BuildInterface;
	
	/**
	 * @param bool $success
	 * @return $this
	 */
	public function setSuccess(bool $success);
	
	/**
	 * @return int|string|null
	 */
	public function getJobDescriptionCommentId();
	
	/**
	 * @param $id
	 * @return $this
	 */
	public function setJobDescriptionCommentId($id);

	/**
	 * @return ParameterBagInterface
	 */
	public function getParameters() : ParameterBagInterface;
}
