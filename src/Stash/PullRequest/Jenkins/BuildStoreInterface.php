<?php

namespace Stash\PullRequest\Jenkins;

use Stash\PullRequest\Jenkins\Exception\BuildNotFoundException;

/**
 * Interface BuildStoreInterface
 * @package Stash\PullRequest\Jenkins
 */
interface BuildStoreInterface
{
	/**
	 * @param string|int $pullRequestId
	 * @throws BuildNotFoundException
	 * @return BuildInterface
	 */
	public function load($pullRequestId) : BuildInterface;
	
	/**
	 * @param BuildInterface $build
	 * @return bool
	 */
	public function save(BuildInterface $build);
	
	/**
	 * @param $pullRequestId
	 * @return bool
	 */
	public function has($pullRequestId) : bool;

    /**
     * @param BuildCollectionInterface $collection
     * @return bool
     */
	public function cleanBuildsWhichIsNotInList(BuildCollectionInterface $collection);
}
