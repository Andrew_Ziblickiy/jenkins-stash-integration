<?php

namespace Stash\PullRequest\Jenkins;

/**
 * Class BuildCollectionInterface
 * @package Stash\PullRequest\Jenkins
 */
interface BuildCollectionInterface
{
	/**
	 * @param BuildInterface $build
	 * @return BuildCollectionInterface
	 */
	public function addBuild(BuildInterface $build) : BuildCollectionInterface;

	/**
	 * @param BuildInterface $build
	 * @return void
	 */
	public function removeBuild(BuildInterface $build);
	
	/**
	 * @return BuildInterface[]
	 */
	public function getBuilds();
}
