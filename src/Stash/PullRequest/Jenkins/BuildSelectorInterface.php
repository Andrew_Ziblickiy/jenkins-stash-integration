<?php

namespace Stash\PullRequest\Jenkins;

use Stash\PullRequest\Jenkins\Exception\BuildNotFoundException;

/**
 * Interface BuildSelectionInterface
 * @package Stash\PullRequest\Jenkins
 */
interface BuildSelectorInterface
{
	/**
	 * @return string
	 */
	public function getStrategyName() : string;
	
	/**
	 * @param BuildCollectionInterface $collection
	 * @throws BuildNotFoundException
	 * @return BuildInterface
	 */
	public function select(BuildCollectionInterface $collection) : BuildInterface;
}
