<?php

namespace Stash\PullRequest\Jenkins;

use Stash\Commit\BuildStatus\BuildStatusAwareInterface;
use Stash\Commit\BuildStatus\BuildStatusAwareTrait;
use Stash\Commit\BuildStatus\BuildStatusesAwareInterface;
use Stash\Commit\BuildStatus\BuildStatusInterface;
use Stash\Property\NotPassedInterface;
use Stash\PullRequest\PullRequestAwareInterface;
use Stash\PullRequest\PullRequestAwareTrait;
use Stash\PullRequest\PullRequestInterface;
use Stash\PullRequest\UndefinedPullRequest;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Class Build
 * @package Stash\PullRequest\Jenkins
 */
class Build implements BuildInterface, BuildStatusAwareInterface, PullRequestAwareInterface, BuildStatusesAwareInterface
{
	use BuildStatusAwareTrait;
	use PullRequestAwareTrait;
	
	/**
	 * @var int
	 */
	protected $pullRequestId;
	
	/**
	 * @var string
	 */
	protected $sourceBranchCommit;
	
	/**
	 * @var string
	 */
	protected $destinationBranchCommit;
	
	/**
	 * @var int
	 */
	protected $buildTimestamp = 0;
	
	/**
	 * @var bool
	 */
	protected $inProgress = false;
	
	/**
	 * @var bool
	 */
	protected $completed = false;
	
	/**
	 * @var bool
	 */
	protected $built = false;
	
	/**
	 * @var bool
	 */
	protected $success = false;
	
	/**
	 * @var string
	 */
	protected $sourceBranch;
	
	/**
	 * @var string
	 */
	protected $destinationBranch;
	
	/**
	 * @var int|string
	 */
	protected $jobDescriptionCommentId;

    /**
     * @var BuildStatusInterface[]
     */
	protected $buildStatuses = [];

	/**
	 * @var ParameterBagInterface
	 */
	protected $parameters;

	/**
	 * Build constructor.
	 * @param PullRequestInterface $pullRequest
	 */
	public function __construct(PullRequestInterface $pullRequest)
	{
		$this->pullRequestId = $pullRequest->getId();

        $fromRef = $pullRequest->getFromRef();
        $dstRef = $pullRequest->getToRef();
        $this->sourceBranchCommit = $fromRef->getLatestChangeset();
        $this->destinationBranchCommit = $dstRef->getLatestChangeset();
        $this->sourceBranch = $fromRef->getDisplayId();
        $this->destinationBranch = $dstRef->getDisplayId();

        $this->getParameters()->add([
            'PULL_REQUEST_ID' => $this->pullRequestId,
            'PULL_REQUEST_FROM_BRANCH' => $this->sourceBranch,
            'PULL_REQUEST_TO_BRANCH' => $this->destinationBranch,
            'PULL_REQUEST_FROM_COMMIT' => $this->sourceBranchCommit,
            'PULL_REQUEST_TO_COMMIT' => $this->destinationBranchCommit
        ]);
	}
	
	/**
	 * @return PullRequestInterface
	 */
	public function getPullRequest() : PullRequestInterface
	{
		if (null === $this->pullRequest) {
			$this->pullRequest = new UndefinedPullRequest($this->getPullRequestId());
		}
		
		return $this->pullRequest;
	}

    /**
	 * @return int
	 */
	public function getPullRequestId(): int
	{
		return $this->pullRequestId;
	}
	
	/**
	 * @return int
	 */
	public function getBuildTimestamp(): int
	{
		return $this->buildTimestamp;
	}
	
	/**
	 * @return string
	 */
	public function getSourceBranchCommit(): string
	{
		return $this->sourceBranchCommit;
	}
	
	/**
	 * @param string $sourceBranchCommit
	 * @return BuildInterface
	 */
	public function setSourceBranchCommit(string $sourceBranchCommit) : BuildInterface
	{
		$this->sourceBranchCommit = $sourceBranchCommit;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getDestinationBranchCommit(): string
	{
		return $this->destinationBranchCommit;
	}
	
	/**
	 * @param string $commit
	 * @return BuildInterface
	 */
	public function setDestinationBranchCommit(string $commit): BuildInterface
	{
		$this->destinationBranchCommit = $commit;
		
		return $this;
	}
	
	/**
	 * @param int $timestamp
	 * @return $this
	 */
	public function setBuildTimestamp(int $timestamp)
	{
		$this->buildTimestamp = $timestamp;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getSourceBranch(): string
	{
		return $this->sourceBranch;
	}
	
	/**
	 * @return string
	 */
	public function getDestinationBranch(): string
	{
		return $this->destinationBranch;
	}
	
	/**
	 * @return bool
	 */
	public function isInProgress(): bool
	{
		return $this->inProgress;
	}
	
	/**
	 * @return bool
	 */
	public function isCompleted(): bool
	{
		return $this->completed;
	}
	
	/**
	 * @return bool
	 */
	public function isBuilt(): bool
	{
		return $this->built;
	}
	
	/**
	 * @return bool
	 */
	public function isSuccess(): bool
	{
		return $this->success;
	}
	
	/**
	 * @return BuildInterface
	 */
	public function start(): BuildInterface
	{
		$this->inProgress = true;
		$this->success = false;
		
		return $this;
	}
	
	/**
	 * @return BuildInterface
	 */
	public function stop(): BuildInterface
	{
		$this->inProgress = false;
		$this->built = true;
		$this->buildTimestamp = time();

        $status = $this->getBuildStatus();

        if ($status) {
            $this->addBuildStatus($status);
        }

		return $this;
	}
	
	/**
	 * @param bool $success
	 * @return $this
	 */
	public function setSuccess(bool $success)
	{
		$this->success = $success;
		
		return $this;
	}
	
	/**
	 * @return int|string
	 */
	public function getJobDescriptionCommentId()
	{
		return $this->jobDescriptionCommentId;
	}
	
	/**
	 * @param $id
	 * @return $this
	 */
	public function setJobDescriptionCommentId($id)
	{
		$this->jobDescriptionCommentId = $id;
		
		return $this;
	}

    /**
     * @param BuildStatusInterface $buildStatus
     * @return $this
     */
    public function addBuildStatus(BuildStatusInterface $buildStatus)
    {
        $this->buildStatuses[] = $buildStatus;

        return $this;
    }

    /**
     * @return BuildStatusInterface[]
     */
    public function getBuildStatuses(): array
    {
        return $this->buildStatuses;
    }

	/**
	 * @return ParameterBagInterface
	 */
	public function getParameters(): ParameterBagInterface
	{
		if (null == $this->parameters) {
			$this->parameters = new ParameterBag();
		}

		return $this->parameters;
	}

	/**
	 * @return string
	 */
	public function serialize()
	{
		return serialize([
			'pullRequestId' => $this->getPullRequestId(),
			'buildTimestamp' => $this->getBuildTimestamp(),
			'inProgress' => $this->isInProgress(),
			'success' => $this->isSuccess(),
			'sourceBranchCommit' => $this->getSourceBranchCommit(),
			'destinationBranchCommit' => $this->getDestinationBranchCommit(),
			'built' => $this->isBuilt(),
			'parameters' => $this->getParameters()->all(),
			'completed' => $this->isCompleted(),
			'sourceBranch' => $this->getSourceBranch(),
			'destinationBranch' => $this->getDestinationBranch(),
			'jobDescriptionCommentId' => $this->getJobDescriptionCommentId(),
			'buildStatus' => $this->getBuildStatus(),
            'buildStatuses' => $this->getBuildStatuses()
		]);
	}
	
	/**
	 * @param string $serialized
	 */
	public function unserialize($serialized)
	{
		$attrs = unserialize($serialized);

		if (isset($attrs['parameters'])) {
			$this->getParameters()->add($attrs['parameters']);
			unset($attrs['parameters']);
		}
		
		foreach ($attrs as $attr => $value) {
			$this->$attr = $value;
		}
	}
}
