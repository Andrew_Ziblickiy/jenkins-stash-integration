<?php

namespace Stash\PullRequest\Jenkins\Selector;

use Stash\PullRequest\Jenkins\BuildCollectionInterface;
use Stash\PullRequest\Jenkins\BuildInterface;
use Stash\PullRequest\Jenkins\BuildSelectorInterface;
use Stash\PullRequest\Jenkins\Exception\BuildNotFoundException;

/**
 * Class SelectorChooser
 * @package Stash\PullRequest\Jenkins\Selector
 */
class SelectorStrategy implements BuildSelectorInterface
{
	/**
	 * @var BuildSelectorInterface[]
	 */
	protected $selectors = [];
	
	/**
	 * @var string
	 */
	protected $strategy;
	
	/**
	 * SelectorStrategy constructor.
	 * @param string $strategy
	 */
	public function __construct(string $strategy = 'new.changed.failed.built')
	{
		$this->strategy = $strategy;
	}
	
	/**
	 * @param BuildSelectorInterface $selector
	 * @return $this
	 */
	public function addBuildSelector(BuildSelectorInterface $selector)
	{
		$this->selectors[$selector->getStrategyName()] = $selector;
		
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getStrategyName(): string
	{
		return 'strategy';
	}
	
	/**
	 * @param BuildCollectionInterface $collection
	 * @return BuildInterface
	 * @throws BuildNotFoundException|\RuntimeException
	 */
	public function select(BuildCollectionInterface $collection): BuildInterface
	{
		if (!isset($this->selectors[$this->strategy])) {
			throw new \RuntimeException('Selector not found for strategy ' . $this->strategy);
		}
		
		return $this->selectors[$this->strategy]->select($collection);
	}
}
