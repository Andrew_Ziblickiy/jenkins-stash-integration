<?php

namespace Stash\PullRequest\Jenkins\Selector;

use Stash\Property\NotPassedInterface;
use Stash\PullRequest\Jenkins\BuildCollectionInterface;
use Stash\PullRequest\Jenkins\BuildInterface;
use Stash\PullRequest\Jenkins\BuildSelectorInterface;
use Stash\PullRequest\Jenkins\Exception\BuildNotFoundException;
use Stash\PullRequest\PullRequestAwareInterface;
use Stash\PullRequest\PullRequestInterface;

/**
 * Class NewChangedFailedBuiltSelector
 * @package Stash\PullRequest\Jenkins\Selector
 */
class NewChangedFailedBuiltSelector implements BuildSelectorInterface
{
	/**
	 * @return string
	 */
	public function getStrategyName(): string
	{
		return 'new.changed.failed.built';
	}

	/**
	 * @param BuildCollectionInterface $collection
	 * @throws BuildNotFoundException
	 * @return BuildInterface
	 */
	public function select(BuildCollectionInterface $collection): BuildInterface
	{
		$builds = $collection->getBuilds();
		$count = count($builds);

		$this->log("Builds count: " . $count);

		$isWip = function (BuildInterface $build) {
			if ($build instanceof PullRequestAwareInterface) {
				$pr = $build->getPullRequest();
				if ($pr instanceof NotPassedInterface) {
					return false;
				}

				if ($this->isWIPPullRequest($pr)) {
					return true;
				}
			}
			return false;
		};

		$this->log("Check on WIP and URG");
		// check on wip and urg
		for ($i = 0; $i < $count; ++$i) {
			$build = $builds[$i];

			if ($build->isInProgress()) {
			    continue;
            }

			$pullRequest = null;
			if ($build instanceof PullRequestAwareInterface) {
				$pr = $build->getPullRequest();
				if (!$pr instanceof NotPassedInterface) {
				    $this->log("Checking pr #" . $pr->getId() . ', ' . $pr->getTitle());
					$pullRequest = $pr;
                    if ($this->isWIPPullRequest($pullRequest)) {
                        $this->log("Is WIP continue");
                        continue;
                    }

                    if ($this->isURGPullRequest($pullRequest)) {

                        $this->log("Is URG");
						if (!$build->isBuilt()) {
						    $this->log("Build was not build, stop searching return this pr #" . $pr->getId());
							return $build;
						}

                        if ($this->isPullRequestHasChanges($build, $pullRequest) && !$build->isInProgress()) {
						    $this->log("Pull request has changes, return this pr");
                            return $build;
                        }
                    }
				}
			}
		}

		$this->log("Regular cycle");
		for ($i = 0; $i < $count; ++$i) {
			$build = $builds[$i];

			if ($build->isInProgress()) {
			    continue;
            }

			$pullRequest = null;
			$this->log("Checking #" . $build->getPullRequestId());
			if ($build instanceof PullRequestAwareInterface) {
				$pr = $build->getPullRequest();
				if (!$pr instanceof NotPassedInterface) {
					$pullRequest = $pr;

                    if ($this->isWIPPullRequest($pullRequest)) {
                        $this->log("Is WIP, continue");
                        continue;
                    }
				}
			}


			//  Check if build already built
			if ($build->isBuilt()) {
			    $this->log("Pull request was built");

			    $this->log("Checking pull requests which were not built");
				//  if it is already built we need to check if some pr in queue which not built yet
				for ($k = $i; $k < $count; ++$k) {
					$nestedBuild = $builds[$k];
					$this->log("Checking #" . $nestedBuild->getPullRequestId());

					if (!$nestedBuild->isBuilt() && !$nestedBuild->isInProgress()) {
					    $this->log("Pull  request was not build or he not in progress");
						if ($isWip($nestedBuild)) {
						    $this->log("Is WIP skipping");
							continue;
						}
						$this->log("Return pr #" . $nestedBuild->getPullRequestId());
						return $nestedBuild;
					}
				}

				//  check current build maybe exists some branch changes
				if ($pullRequest && $this->isPullRequestHasChanges($build, $pullRequest)) {
				    $this->log("Pull request has changed, returning #" . $pullRequest->getId());
					//  yes we can build this pr
					return $build;
				}

//				for ($k = $i; $k < $count; ++$k) {
//					$nestedBuild = $builds[$k];
//					//  todo can be improved
//					if ($nestedBuild instanceof PullRequestAwareInterface) {
//						if (!$nestedBuild instanceof NotPassedInterface) {
//							$nestedPullRequest = $nestedBuild->getPullRequest();
//							if ($isWip($nestedBuild)) {
//								continue;
//							}
//							if ($this->isPullRequestHasChanges($nestedBuild, $nestedPullRequest)) {
//								return $nestedBuild;
//							}
//						}
//					}
//				}

			} else {
			    $this->log("This pull request was not build, returning");
				return $build;
			}
		}
		throw new BuildNotFoundException();
	}

	protected function log(string $msg)
    {
        //echo $msg . PHP_EOL;
        return $this;
    }

    /**
     * @param BuildInterface $build
     * @param PullRequestInterface $pullRequest
     *
     * @return bool
     */
	protected function isPullRequestHasChanges(BuildInterface $build, PullRequestInterface $pullRequest) : bool
    {
        return $build->getSourceBranchCommit() != $pullRequest->getFromRef()->getLatestChangeset() ||
            $build->getDestinationBranchCommit() != $pullRequest->getToRef()->getLatestChangeset();
    }

    /**
     * @param PullRequestInterface $pullRequest
     *
     * @return bool
     */
	protected function isWIPPullRequest(PullRequestInterface $pullRequest) : bool
    {
        $title = $pullRequest->getTitle();

        return strpos($title, '[WIP]') !== false || strpos($title, 'WIP') !== false;
    }

    /**
     * @param PullRequestInterface $pullRequest
     *
     * @return bool
     */
    protected function isURGPullRequest(PullRequestInterface $pullRequest) : bool
    {
        $title = $pullRequest->getTitle();

        return strpos($title, '[URG]') !== false || strpos($title, 'URG') !== false;
    }
}
