<?php

namespace Stash\PullRequest\Jenkins;

/**
 * Interface BuildHistoryInterface
 * @package Stash\PullRequest\Jenkins
 */
interface BuildHistoryInterface
{
	public function addBuild(BuildInterface $build) : BuildHistoryInterface;
}
