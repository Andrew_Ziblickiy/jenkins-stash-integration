<?php

namespace Stash\PullRequest\Jenkins;

/**
 * Interface BuildAwareInterface
 * @package Stash\PullRequest\Jenkins
 */
interface BuildAwareInterface
{
	/**
	 * @return BuildInterface
	 */
	public function getBuild() : BuildInterface;
}
