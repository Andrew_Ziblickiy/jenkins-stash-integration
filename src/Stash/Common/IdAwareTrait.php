<?php

namespace Stash\Common;

/**
 * Class IdAwareTrait
 * @package Stash\Common
 */
trait IdAwareTrait
{
	/**
	 * @var int|string
	 */
	protected $id;
	
	/**
	 * @return int|string
	 */
	public function getId()
	{
		return $this->id;
	}
	
	/**
	 * @param $id
	 * @return $this
	 */
	public function setId($id)
	{
		$this->id = $id;
		
		return $this;
	}
}
