<?php

namespace Stash\Common;

/**
 * Class IdAwareInterface
 * @package Stash\Common
 */
interface IdAwareInterface
{
	/**
	 * @return string|int
	 */
	public function getId();
	
	/**
	 * @param $id
	 * @return $this
	 */
	public function setId($id);
}
