<?php

require __DIR__.'/../../vendor/autoload.php';

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\EventDispatcher\DependencyInjection\RegisterListenersPass;
use Symfony\Component\EventDispatcher\EventDispatcher;

$container = new ContainerBuilder();
$container->addCompilerPass(new RegisterListenersPass());
$container->register('event_dispatcher', EventDispatcher::class);

$container->setParameter('cache.root', __DIR__ . '/cache');
$container->setParameter('cache.config.root', $container->getParameter('cache.root') . '/config');
$container->setParameter('resource.root', __DIR__ . '/resources');

$loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/resources/config'));
$loader->load('parameters.yml');
$loader->load('services.yml');

try {
	$loader = new YamlFileLoader($container, new FileLocator($container->getParameter('cache.config.root')));
	$loader->load('parameters.yml');
	$loader->load('services.yml');
} catch (\Exception $e) {
	//  it seems like config not found
	//  trying to load variables with getenv
	$parametersForCheck = [
		'stash.api.host', 'stash.api.version', 'stash.api.username', 'stash.api.password', 'stash.project.key',
		'stash.project.name'
	];
	foreach ($parametersForCheck as $param) {
		$envParam = strtoupper(str_replace('.', '_', $param));
		$value = getenv($envParam);
		if ($value) {
			$container->setParameter($param, $value);
		}
	}
}

$container->compile();

return $container;